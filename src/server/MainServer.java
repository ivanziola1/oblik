package server;

import domain.Mark;
import domain.Student;
import domain.Subject;
import manager.*;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class MainServer {

    private static final MarkManagerInterface markManager = new MarkManager();
    private static final SubjectManagerInterface subjectManager = new SubjectManager();
    private static final StudentManagerInterface studentManager = new StudentManager();
    private static final RequestManagerInterface requestManager = new RequestManager();

    public static void main(String[] args) throws Exception {
        ServerSocket serverSocket = new ServerSocket(9999);

        while (true) {

            Socket socket = serverSocket.accept();
            // RemoteRequestMessage
            ObjectInputStream inFromClient = new ObjectInputStream(
                    socket.getInputStream());
            RemoteRequestMessage msg = (RemoteRequestMessage) inFromClient
                    .readObject();
            System.out.println("** Received call for " + msg.getService() + "."
                    + msg.getMethodName() + "()");

            RemoteResponseMessage response = handleRequest(msg);
            ObjectOutputStream outToClient = new ObjectOutputStream(
                    socket.getOutputStream());
            outToClient.writeObject(response);
        }
    }

    private static RemoteResponseMessage handleRequest(RemoteRequestMessage msg) {

        if (MarkManagerInterface.class.getSimpleName().equals(msg.getService())) {
            return handleMarkRequest(msg);
        }
        if (SubjectManagerInterface.class.getSimpleName().equals(
                msg.getService())) {
            return handleSubjectRequest(msg);
        }
        if (StudentManagerInterface.class.getSimpleName().equals(
                msg.getService())) {
            return handleStudentRequest(msg);
        }
        if (RequestManagerInterface.class.getSimpleName().equals(
                msg.getService())) {
            return handleReqRequest(msg);
        }

        return null;

    }

    /**
     * SubjectManager.
     *
     * @param msg
     * @return
     */

    private static RemoteResponseMessage handleSubjectRequest(
            RemoteRequestMessage msg) {
        RemoteResponseMessage result = new RemoteResponseMessage();

        if (msg.getMethodName().equals("getAllSubjects")) {
            try {
                result.setResult(subjectManager.getAllSubjects());
            } catch (Exception e) {
                result.setException(e);
            }
        } else if (msg.getMethodName().equals("getSortedSubjects")) {
            try {
                result.setResult(subjectManager.getSortedSubjects((String) msg.getArguments()[0], (String) msg.getArguments()[1]));
            } catch (Exception e) {
                result.setException(e);
            }
        } else if (msg.getMethodName().equals("createSubject")) {
            try {
                result.setResult(subjectManager.createSubject((Subject) msg
                        .getArguments()[0]));
            } catch (Exception e) {
                result.setException(e);
            }
        } else if (msg.getMethodName().equals("removeSubject")) {
            try {
                subjectManager.removeSubject((Integer) msg.getArguments()[0]);
            } catch (Exception e) {
                result.setException(e);
            }
        } else if (msg.getMethodName().equals("findById")) {
            try {
                result.setResult(subjectManager.findById((Integer) msg
                        .getArguments()[0]));
            } catch (Exception e) {
                result.setException(e);
            }
        } else if (msg.getMethodName().equals("updateSubject")) {
            try {
                subjectManager.updateSubject((Subject) msg.getArguments()[0]);
            } catch (Exception e) {
                result.setException(e);
            }
        }
        return result;
    }

    /**
     * StudentManager.
     *
     * @param msg
     * @return
     */

    private static RemoteResponseMessage handleStudentRequest(
            RemoteRequestMessage msg) {
        RemoteResponseMessage result = new RemoteResponseMessage();

        if (msg.getMethodName().equals("getSortedSubjects")) {
            try {
                result.setResult(studentManager.getSortedSubjects((String) msg.getArguments()[0], (String) msg.getArguments()[1]));
            } catch (Exception e) {
                result.setException(e);
            }
        } else if (msg.getMethodName().equals("getAllStudents")) {
            try {
                result.setResult(studentManager.getAllStudents());
            } catch (Exception e) {
                result.setException(e);
            }
        } else if (msg.getMethodName().equals("findByGroup")) {
            try {
                result.setResult(studentManager.findByGroup((String) msg
                        .getArguments()[0]));
            } catch (Exception e) {
                result.setException(e);
            }
        } else if (msg.getMethodName().equalsIgnoreCase("findById")) {
            try {
                result.setResult(studentManager.findById((Integer) msg
                        .getArguments()[0]));
            } catch (Exception e) {
                result.setException(e);
            }
        } else if (msg.getMethodName().equals("createStudent")) {
            try {
                result.setResult(studentManager.createStudent((Student) msg.getArguments()[0]));
            } catch (Exception e) {
                result.setException(e);
            }
        } else if (msg.getMethodName().equals("removeStudent")) {
            try {
                studentManager.removeStudent((Integer) msg.getArguments()[0]);
            } catch (Exception e) {
                result.setException(e);
            }
        } else if (msg.getMethodName().equals("getImage")) {
            try {
                studentManager.getImage((Integer) msg.getArguments()[0]);
            } catch (Exception e) {
                result.setException(e);
            }
        } else if (msg.getMethodName().equals("updateStudent")) {
            try {
                studentManager.updateStudent((Student) msg.getArguments()[0]);
            } catch (Exception e) {
                result.setException(e);
            }

        }
        return result;
    }

    /**
     * MarkManager.
     *
     * @param msg
     * @return
     */

    private static RemoteResponseMessage handleMarkRequest(
            RemoteRequestMessage msg) {
        RemoteResponseMessage result = new RemoteResponseMessage();
        if (msg.getMethodName().equals("getAllMarks")) {
            try {
                result.setResult(markManager.getAllMarks());
            } catch (Exception e) {
                result.setException(e);
            }
        } else if (msg.getMethodName().equals("getAllStudentMarks")) {
            try {
                result.setResult(markManager.getAllStudentMarks((Integer) msg
                        .getArguments()[0]));
            } catch (Exception e) {
                result.setException(e);
            }
        } else if (msg.getMethodName().equals("getSortedMarks")) {
            try {
                result.setResult(markManager.getSortedMarks((String) msg.getArguments()[0], (String) msg.getArguments()[1]));
            } catch (Exception e) {
                result.setException(e);
            }
        } else if (msg.getMethodName().equals("getAllSubjectMarks")) {
            try {
                result.setResult(markManager.getAllSubjectMarks((Integer) msg
                        .getArguments()[0]));
            } catch (Exception e) {
                result.setException(e);
            }
        } else if (msg.getMethodName().equals("createMark")) {
            try {
                result.setResult(markManager.createMark((Mark) msg
                        .getArguments()[0]));
            } catch (Exception e) {
                result.setException(e);
            }
        } else if (msg.getMethodName().equalsIgnoreCase("removeMark")) {
            try {
                markManager.removeMark((Integer) msg.getArguments()[0]);
            } catch (Exception e) {
                result.setException(e);
            }
        } else if (msg.getMethodName().equalsIgnoreCase("findById")) {
            try {
                result.setResult(markManager.findById((Integer) msg
                        .getArguments()[0]));
            } catch (Exception e) {
                result.setException(e);
            }
        } else if (msg.getMethodName().equals("updateMark")) {
            try {
                markManager.updateMark((Mark) msg.getArguments()[0]);
            } catch (Exception e) {
                result.setException(e);
            }
        }
        return result;
    }

    private static RemoteResponseMessage handleReqRequest(
            RemoteRequestMessage msg) {
        RemoteResponseMessage result = new RemoteResponseMessage();
        if (msg.getMethodName().equals("getAllSubjectMarks")) {
            try {
                result.setResult(requestManager.getAllSubjectMarks((Integer) msg
                        .getArguments()[0]));
            } catch (Exception e) {
                result.setException(e);
            }
        } else if (msg.getMethodName().equals("getAllStudentsAVGMarks")) {
            try {
                result.setResult(requestManager.getAllStudentsAVGMarks());
            } catch (Exception e) {
                result.setException(e);
            }
        } else if (msg.getMethodName().equals("getAllSchollars")) {
            try {
                result.setResult(requestManager.getAllSchollars());
            } catch (Exception e) {
                result.setException(e);
            }
        }
        return result;
    }
}
