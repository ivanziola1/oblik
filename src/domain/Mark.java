package domain;

public class Mark extends DomainType {

    /**
     * default value for serialVersionUID;
     */
    private static final long serialVersionUID = 1L;
    private Integer subjectId;
    private Integer studentId;
    private String date;
    private Integer mark;
    private String classesType;
    private Integer retake;

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public Integer getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getMark() {
        return mark;
    }

    public void setMark(Integer mark) {
        this.mark = mark;
    }

    public String getClassesType() {
        return classesType;
    }

    public void setClassesType(String classesType) {
        this.classesType = classesType;
    }

    public Integer getRetake() {
        return retake;
    }

    public void setRetake(Integer retake) {
        this.retake = retake;
    }
 public  String toString()
 {
     return id+ " "+ mark+"  "+ retake;
 }

}
