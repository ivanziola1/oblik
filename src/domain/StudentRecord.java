package domain;

import java.io.Serializable;

/**
 * Created by admin on 23.04.15.
 */
public class StudentRecord implements Serializable {
    private static final long serialVersionUID = -7873357187538188983L;
    private String subjectName;
    private String lastname;
    private String firstname;
    private String recordBook;
    private String group;
    private Float mark;

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setRecordBook(String recordBook) {
        this.recordBook = recordBook;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public void setMark(Float mark) {
        this.mark = mark;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public String getLastname() {
        return lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getGroup() {
        return group;
    }

    public String getRecordBook() {
        return recordBook;
    }

    public Float getMark() {
        return mark;
    }


}
