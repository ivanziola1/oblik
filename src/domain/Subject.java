package domain;

public class Subject extends DomainType {

    /**
     * default value for serialVersionUID;
     */
    private static final long serialVersionUID = 1L;
    private String subjectName;
    private String teacherName;
    private Float ects;
    private Integer hours;
    private Integer lectures;
    private Integer practicalTraining;
    private Integer laboratoryWork;
    private Integer consultation;
    private String formOfControl;

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public Float getEcts() {
        return ects;
    }

    public void setEcts(Float ects) {
        this.ects = ects;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    public Integer getLectures() {
        return lectures;
    }

    public void setLectures(Integer lectures) {
        this.lectures = lectures;
    }

    public Integer getPracticalTraining() {
        return practicalTraining;
    }

    public void setPracticalTraining(Integer practicalTraining) {
        this.practicalTraining = practicalTraining;
    }

    public Integer getConsultation() {
        return consultation;
    }

    public void setConsultation(Integer consultation) {
        this.consultation = consultation;
    }

    public Integer getLaboratoryWork() {
        return laboratoryWork;
    }

    public void setLaboratoryWork(Integer laboratoryWork) {
        this.laboratoryWork = laboratoryWork;
    }

    public String getFormOfControl() {
        return formOfControl;
    }

    public void setFormOfControl(String formOfControl) {
        this.formOfControl = formOfControl;
    }

    public String toString() {
        return subjectName + " ";
    }

}
