package domain;

import java.io.Serializable;

/**
 * Created by admin on 14.04.15.
 */
public class TeacherRecord implements Serializable {
    private Integer subjectId;
    private String lastname;
    private String firstname;
    private String middlename;
    private Integer mark;
    private String date;
    private Integer retake;

    public Integer getSubjectId() {
        return subjectId;
    }

    public String getLastname() {
        return lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public Integer getMark() {
        return mark;
    }

    public String getDate() {
        return date;
    }

    public Integer getRetake() {
        return retake;
    }

    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public void setMark(Integer mark) {
        this.mark = mark;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setRetake(Integer retake) {
        this.retake = retake;
    }

    public String toString() {
        return lastname + " " + firstname + " " + middlename + " " + mark;
    }
}
