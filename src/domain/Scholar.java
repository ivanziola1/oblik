package domain;

import java.io.Serializable;

/**
 * Created by admin on 29.04.15.
 */
public class Scholar implements Serializable {
    private String lastname;
    private String firstname;
    private String recordBook;
    private Float avgMark;
    private Integer scholarship;

    public void setAvgMark(Float avgMark) {
        this.avgMark = avgMark;
    }


    public Float getAvgMark() {
        return avgMark;
    }


    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setRecordBook(String recordBook) {
        this.recordBook = recordBook;
    }

    public void setScholarship(Integer scholarship) {
        this.scholarship = scholarship;
    }

    public String getLastname() {
        return lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getRecordBook() {
        return recordBook;
    }

    public Integer getScholarship() {
        return scholarship;
    }

    public String toString() {
        return lastname + " " + firstname + " " + scholarship;
    }
}
