

import gui.LogoForm;
import gui.MainForm;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainClient {
    static String windows = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
    static LogoForm logoForm = new LogoForm();
    static MainForm mainForm = new MainForm();

    static javax.swing.Timer timer = new javax.swing.Timer(0,
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {

                    logoForm.setVisible(false);
                    mainForm.setVisible(true);
                    timer.stop();
                }
            });

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(windows);
        } catch (Exception e2) {
            e2.printStackTrace();
        }

        logoForm.setVisible(true);
        timer.start();
        JFrame.setDefaultLookAndFeelDecorated(false);
        JDialog.setDefaultLookAndFeelDecorated(false);
        mainForm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

}
