/**
 *
 */
package manager;

import domain.Subject;

import java.util.List;

/**
 * @author admin
 */
public interface SubjectManagerInterface {
    int createSubject(Subject subject) throws Exception;

    public List<Subject> getSortedSubjects(String columnname, String orderVal) throws Exception;

    void updateSubject(Subject subject) throws Exception;

    void removeSubject(int subjectId) throws Exception;

    Subject findById(int subjectId) throws Exception;

    List<Subject> getAllSubjects() throws Exception;
}
