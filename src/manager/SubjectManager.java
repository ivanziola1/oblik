/**
 *
 */
package manager;

import dao.SubjectDao;
import domain.Subject;

import java.util.List;

/**
 * @author admin
 */
public class SubjectManager implements SubjectManagerInterface {
    private SubjectDao subjectDao = new SubjectDao();

    /*
     * (non-Javadoc)
     *
     * @see manager.SubjectManagerInterface#createSubject(domain.Subject)
     */
    @Override
    public int createSubject(Subject subject) throws Exception {
        validate(subject);
        return subjectDao.insertSubject(subject);
    }

    @Override
    public List<Subject> getSortedSubjects(String columnname, String orderVal) throws Exception {
        return subjectDao.getSortedSubjects(columnname, orderVal);
    }

    /*
     * (non-Javadoc)
     *
     * @see manager.SubjectManagerInterface#updateSubject(domain.Subject)
     */
    @Override
    public void updateSubject(Subject subject) throws Exception {
        validate(subject);
        subjectDao.updateSubject(subject);
    }

    /*
     * (non-Javadoc)
     *
     * @see manager.SubjectManagerInterface#removeSubject(int)
     */
    @Override
    public void removeSubject(int subjectId) throws Exception {
        subjectDao.deleteSubject(subjectId);

    }

    /*
     * (non-Javadoc)
     *
     * @see manager.SubjectManagerInterface#getAllSubjects()
     */
    @Override
    public List<Subject> getAllSubjects() throws Exception {
        return subjectDao.findAll();
    }

    @Override
    public Subject findById(int subjectId) throws Exception {
        return subjectDao.findById(subjectId);
    }

    void validate(Subject subject) {
        if (subject == null) {
            throw new IllegalArgumentException(
                    "Помилка");
        }
        if (subject.getSubjectName() == null
                || subject.getSubjectName().length() == 0) {
            throw new IllegalArgumentException(
                    "Назва предмету обовязкове");
        } else if (subject.getSubjectName() != null
                && subject.getSubjectName().length() > 100) {
            throw new IllegalArgumentException(
                    "Назва предмету обовязкове повинно бути < 100");
        }
        if (subject.getTeacherName() != null
                && subject.getTeacherName().length() > 100) {
            throw new IllegalArgumentException(
                    "Ім'я викладача повинно бути < 100");

        }


    }

}
