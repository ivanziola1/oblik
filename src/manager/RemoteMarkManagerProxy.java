package manager;

import domain.Mark;
import server.RemoteRequestMessage;
import server.RemoteResponseMessage;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;

public class RemoteMarkManagerProxy implements MarkManagerInterface {
    private static final String REMOTE_SERVER_ADDR = "localhost";
    private static final int REMOTE_SERVER_PORT = 9999;

    @Override
    public List<Mark> getSortedMarks(String columnname, String orderVal) throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                MarkManagerInterface.class.getSimpleName(),
                "getSortedMarks", new Object[]{columnname,orderVal}));
        if (msg == null) {
            return null;
        }
        if (msg.getException() != null) {
            throw msg.getException();
        }
        return (List<Mark>) msg.getResult();
    }

    @Override
    public int createMark(Mark mark) throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                MarkManagerInterface.class.getSimpleName(), "createMark",
                new Object[]{mark}));

        if (msg.getException() != null) {
            throw msg.getException();
        }
        return (Integer) msg.getResult();
    }

    @Override
    public void updateMark(Mark mark) throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                MarkManagerInterface.class.getSimpleName(), "updateMark",
                new Object[]{mark}));

        if (msg.getException() != null) {
            throw msg.getException();
        }

    }

    @Override
    public void removeMark(int markId) throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                MarkManagerInterface.class.getSimpleName(), "removeMark",
                new Object[]{markId}));

        if (msg.getException() != null) {
            throw msg.getException();
        }

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Mark> getAllMarks() throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                MarkManagerInterface.class.getSimpleName(), "getAllMarks", null));
        if (msg == null) {
            return null;
        }
        if (msg.getException() != null) {
            throw msg.getException();
        }
        return (List<Mark>) msg.getResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Mark> getAllStudentMarks(int studentId) throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                MarkManagerInterface.class.getSimpleName(),
                "getAllStudentMarks", new Object[]{studentId}));
        if (msg == null) {
            return null;
        }
        if (msg.getException() != null) {
            throw msg.getException();
        }
        return (List<Mark>) msg.getResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Mark> getAllSubjectMarks(int subjectId) throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                MarkManagerInterface.class.getSimpleName(),
                "getAllStudentMarks", new Object[]{subjectId}));
        if (msg == null) {
            return null;
        }
        if (msg.getException() != null) {
            throw msg.getException();
        }
        return (List<Mark>) msg.getResult();
    }

    @Override
    public Mark findById(int markId) throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                MarkManagerInterface.class.getSimpleName(),
                "getAllStudentMarks", new Object[]{markId}));
        if (msg == null) {
            return null;
        }
        if (msg.getException() != null) {
            throw msg.getException();
        }
        return (Mark) msg.getResult();
    }

    private RemoteResponseMessage invokeRemoteCall(
            RemoteRequestMessage remoteRequestMessage) throws Exception {
        // ������� ������ �� ��������
        Socket clientSocket = new Socket(REMOTE_SERVER_ADDR, REMOTE_SERVER_PORT);
        ObjectOutputStream outToServer = new ObjectOutputStream(
                clientSocket.getOutputStream());

        // ������ �� ������ ������������ ���� �� ������� �� ���������
        outToServer.writeObject(remoteRequestMessage);

        // ������ �� ������� ������� �� ��������� ���� ����
        // RemoteResponseMessage
        ObjectInputStream inFromServer = new ObjectInputStream(
                clientSocket.getInputStream());
        Object response = inFromServer.readObject();

        // ������� ������ �� ��������
        clientSocket.close();

        // ������� ������� �������
        return (RemoteResponseMessage) response;
    }

}
