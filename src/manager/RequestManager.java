package manager;

import dao.RequestDao;
import domain.Scholar;
import domain.StudentRecord;
import domain.TeacherRecord;

import java.util.List;

/**
 * Created by admin on 14.04.15.
 */
public class RequestManager implements RequestManagerInterface {
    RequestDao requestDao = new RequestDao();

    @Override
    public List<TeacherRecord> getAllSubjectMarks(int subjectId) throws Exception {
        return requestDao.findAllSubjectMarks(subjectId);
    }

    @Override
    public List<Scholar> getAllSchollars() throws Exception {
        return requestDao.findAllSchollars();
    }

    @Override
    public List<StudentRecord> getAllStudentsAVGMarks() throws Exception {
        return requestDao.findAllStudentsAVGMarks();
    }
}
