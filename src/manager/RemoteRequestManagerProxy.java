package manager;

import domain.Scholar;
import domain.StudentRecord;
import domain.TeacherRecord;
import server.RemoteRequestMessage;
import server.RemoteResponseMessage;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;

/**
 * Created by admin on 14.04.15.
 */
public class RemoteRequestManagerProxy implements RequestManagerInterface {
    private static final String REMOTE_SERVER_ADDR = "localhost";
    private static final int REMOTE_SERVER_PORT = 9999;

    @Override
    public List<TeacherRecord> getAllSubjectMarks(int subjectId) throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                RequestManagerInterface.class.getSimpleName(),
                "getAllSubjectMarks", new Object[]{subjectId}));
        if (msg == null) {
            return null;
        }
        if (msg.getException() != null) {
            throw msg.getException();
        }
        return (List<TeacherRecord>) msg.getResult();
    }

    @Override
    public List<Scholar> getAllSchollars() throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                RequestManagerInterface.class.getSimpleName(),
                "getAllSchollars", null));
        if (msg == null) {
            return null;
        }
        if (msg.getException() != null) {
            throw msg.getException();
        }
        return (List<Scholar>) msg.getResult();
    }


    @Override
    public List<StudentRecord> getAllStudentsAVGMarks() throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                RequestManagerInterface.class.getSimpleName(),
                "getAllStudentsAVGMarks", null));
        if (msg == null) {
            return null;
        }
        if (msg.getException() != null) {
            throw msg.getException();
        }
        return (List<StudentRecord>) msg.getResult();
    }

    private RemoteResponseMessage invokeRemoteCall(RemoteRequestMessage remoteRequestMessage) throws Exception {
        Socket clientSocket = new Socket(REMOTE_SERVER_ADDR, REMOTE_SERVER_PORT);
        ObjectOutputStream outToServer = new ObjectOutputStream(
                clientSocket.getOutputStream());
        outToServer.writeObject(remoteRequestMessage);
        ObjectInputStream inFromServer = new ObjectInputStream(
                clientSocket.getInputStream());
        Object response = inFromServer.readObject();
        clientSocket.close();
        return (RemoteResponseMessage) response;
    }

}
