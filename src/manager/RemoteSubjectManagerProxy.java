/**
 *
 */
package manager;

import domain.Subject;
import server.RemoteRequestMessage;
import server.RemoteResponseMessage;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;

/**
 * @author admin
 */
public class RemoteSubjectManagerProxy implements SubjectManagerInterface {
    private static final String REMOTE_SERVER_ADDR = "localhost";
    private static final int REMOTE_SERVER_PORT = 9999;

    @Override
    public int createSubject(Subject subject) throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                SubjectManagerInterface.class.getSimpleName(), "createSubject",
                new Object[]{subject}));

        if (msg.getException() != null) {
            throw msg.getException();
        }
        return (Integer) msg.getResult();
    }

    @Override
    public List<Subject> getSortedSubjects(String columnname, String orderVal) throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                SubjectManagerInterface.class.getSimpleName(),
                "getSortedSubjects", new Object[]{columnname,orderVal}));
        if (msg == null) {
            return null;
        }
        if (msg.getException() != null) {
            throw msg.getException();
        }
        return (List<Subject>) msg.getResult();
    }

    @Override
    public void updateSubject(Subject subject) throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                SubjectManagerInterface.class.getSimpleName(), "updateSubject",
                new Object[]{subject}));

        if (msg.getException() != null) {
            throw msg.getException();
        }
    }

    @Override
    public void removeSubject(int subjectId) throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                SubjectManagerInterface.class.getSimpleName(), "removeSubject",
                new Object[]{subjectId}));

        if (msg.getException() != null) {
            throw msg.getException();
        }

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Subject> getAllSubjects() throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                SubjectManagerInterface.class.getSimpleName(),
                "getAllSubjects", null));
        if (msg == null) {
            return null;
        }
        if (msg.getException() != null) {
            throw msg.getException();
        }
        return (List<Subject>) msg.getResult();
    }

    @Override
    public Subject findById(int subjectId) throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                SubjectManagerInterface.class.getSimpleName(), "findById",
                new Object[]{subjectId}));
        if (msg == null) {
            return null;
        }
        if (msg.getException() != null) {
            throw msg.getException();
        }
        return (Subject) msg.getResult();
    }

    private RemoteResponseMessage invokeRemoteCall(
            RemoteRequestMessage remoteRequestMessage) throws Exception {
        Socket clientSocket = new Socket(REMOTE_SERVER_ADDR, REMOTE_SERVER_PORT);
        ObjectOutputStream outToServer = new ObjectOutputStream(
                clientSocket.getOutputStream());
        outToServer.writeObject(remoteRequestMessage);
        ObjectInputStream inFromServer = new ObjectInputStream(
                clientSocket.getInputStream());
        Object response = inFromServer.readObject();
        clientSocket.close();
        return (RemoteResponseMessage) response;
    }

}
