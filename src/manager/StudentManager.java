package manager;

import dao.StudentDao;
import domain.Student;

import java.sql.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StudentManager implements StudentManagerInterface {
    private StudentDao studentDao = new StudentDao();
    String regex = "^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9])(?:( [0-2][0-9]):([0-5][0-9]):([0-5][0-9]))?$";
    Pattern pattern = Pattern.compile(regex);
    @Override
    public List<Student> getAllStudents() throws Exception {
        return studentDao.findAll();
    }

    @Override
    public void getImage(int key) throws Exception {
        studentDao.getImage(key);
    }

    @Override
    public List<Student> findByGroup(String group) throws Exception {
        return studentDao.findByGroup(group);
    }

    @Override
    public void removeStudent(int studentId) throws Exception {
        studentDao.deleteStudent(studentId);

    }

    @Override
    public void updateStudent(Student student) throws Exception {
        validateStudent(student);
        studentDao.updateStudent(student);
    }

    @Override
    public List<Student> getSortedSubjects(String columnname, String orderVal) throws Exception {
        return studentDao.getSortedSubjects(columnname,orderVal);
    }

    @Override
    public int createStudent(Student student) throws Exception {
        validateStudent(student);
        return studentDao.insertStudent(student);
    }

    @Override
    public Student findById(int studentId) throws Exception {
        return studentDao.findById(studentId);
    }

    private void validateStudent(Student student) {
        if (student == null) {
            throw new IllegalArgumentException(
                    "Помилка");
        }
        if (student.getLastname() == null
                || student.getLastname().length() == 0) {
            throw new IllegalArgumentException(
                    "Введіть прізвище");
        } else if (student.getLastname() != null
                && student.getLastname().length() > 50) {
            throw new IllegalArgumentException(
                    "Прізвище повинно бути < 50");
        }
        if (student.getFirstname() == null
                || student.getFirstname().length() == 0) {
            throw new IllegalArgumentException(
                    "Введіть ім'я");
        } else if (student.getFirstname() != null
                && student.getFirstname().length() > 50) {
            throw new IllegalArgumentException(
                    "Ім'я повинно бути < 50");
        }
        if (student.getMiddlename() == null
                || student.getMiddlename().length() == 0) {
            throw new IllegalArgumentException(
                    "Введіть по-батькові");
        } else if (student.getMiddlename() != null
                && student.getMiddlename().length() > 50) {
            throw new IllegalArgumentException(
                    "По-батькові повинно бути < 50");
        }
        if (student.getGroup() != null && student.getGroup().length() > 10) {
            throw new IllegalArgumentException(
                    "Група повинна бути <10");
        }
        if (student.getRecordBook() != null && student.getRecordBook().length() > 10) {
            throw new IllegalArgumentException(
                    "Залікова книжка повинна бути <10");
        }
        if (student.getCurator() != null && student.getCurator().length() > 100) {
            throw new IllegalArgumentException(
                    "Ім'я куратора повинно бути < 100");
        }
        if (student.getBirthdayDate() != null) {
            Matcher matcher = pattern.matcher(student.getBirthdayDate());
            if(!matcher.matches())
            throw new IllegalArgumentException(
                    "Дата введена невірно");
        }

    }


}