package manager;

import dao.MarkDao;
import domain.Mark;

import java.util.List;

/**
 * @author admin
 */

public class MarkManager implements MarkManagerInterface {

    private MarkDao markDao = new MarkDao();

    @Override
    public List<Mark> getAllStudentMarks(int studentId) throws Exception {
        return markDao.findAllSTudentMarks(studentId);
    }

    @Override
    public List<Mark> getAllSubjectMarks(int subjectId) throws Exception {
        return markDao.findAllSTudentMarks(subjectId);
    }

    @Override
    public List<Mark> getSortedMarks(String columnname, String orderVal) throws Exception {
        return markDao.getSortedMarks(columnname, orderVal);
    }

    @Override
    public int createMark(Mark mark) throws Exception {
        validateMark(mark);
        return markDao.insertMark(mark);
    }

    @Override
    public void updateMark(Mark mark) throws Exception {
        validateMark(mark);
        markDao.updateMark(mark);

    }

    @Override
    public void removeMark(int markId) throws Exception {
        markDao.deleteMark(markId);
    }

    @Override
    public List<Mark> getAllMarks() throws Exception {
        return markDao.findAll();
    }

    void validateMark(Mark mark) {

    }

    @Override
    public Mark findById(int markId) throws Exception {
        return markDao.findById(markId);
    }

}
