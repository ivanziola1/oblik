package manager;

import domain.Scholar;
import domain.StudentRecord;
import domain.TeacherRecord;

import java.util.List;

/**
 * Created by admin on 14.04.15.
 */
public interface RequestManagerInterface {
    List<TeacherRecord> getAllSubjectMarks(int subjectId) throws Exception;
    List<Scholar> getAllSchollars() throws Exception;
    List<StudentRecord> getAllStudentsAVGMarks() throws Exception;
}
