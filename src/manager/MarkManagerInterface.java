/**
 *
 */
package manager;

import domain.Mark;

import java.util.List;

/**
 * @author admin
 */
public interface MarkManagerInterface {
    public List<Mark> getSortedMarks(String columnname, String orderVal) throws Exception;
    int createMark(Mark mark) throws Exception;

    void updateMark(Mark mark) throws Exception;

    void removeMark(int markId) throws Exception;

    Mark findById(int markId) throws Exception;

    List<Mark> getAllMarks() throws Exception;

    List<Mark> getAllStudentMarks(int studentId) throws Exception;

    List<Mark> getAllSubjectMarks(int subjectId) throws Exception;

}
