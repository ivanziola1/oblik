package manager;

import domain.Student;
import server.RemoteRequestMessage;
import server.RemoteResponseMessage;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;

public class RemoteStudentManagerProxy implements StudentManagerInterface {

    private static final String REMOTE_SERVER_ADDR = "localhost";
    private static final int REMOTE_SERVER_PORT = 9999;

    @Override
    public List<Student> getSortedSubjects(String columnname, String orderVal) throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                StudentManagerInterface.class.getSimpleName(),
                "getSortedSubjects", new Object[]{columnname,orderVal}));
        if (msg == null) {
            return null;
        }
        if (msg.getException() != null) {
            throw msg.getException();
        }
        return (List<Student>) msg.getResult();
    }

    @Override
    public int createStudent(Student student) throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                StudentManagerInterface.class.getSimpleName(), "createStudent",
                new Object[]{student}));

        if (msg.getException() != null) {
            throw msg.getException();
        }

        return (Integer) msg.getResult();
    }

    @Override
    public void updateStudent(Student student) throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                StudentManagerInterface.class.getSimpleName(), "updateStudent",
                new Object[]{student}));

        if (msg.getException() != null) {
            throw msg.getException();
        }

    }

    @Override
    public void removeStudent(int studentId) throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                StudentManagerInterface.class.getSimpleName(), "removeStudent",
                new Object[]{studentId}));

        if (msg.getException() != null) {
            throw msg.getException();
        }

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Student> getAllStudents() throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                StudentManagerInterface.class.getSimpleName(),
                "getAllStudents", null));
        if (msg == null) {
            return null;
        }
        if (msg.getException() != null) {
            throw msg.getException();
        }
        return (List<Student>) msg.getResult();
    }

    @Override
    public void getImage(int key) throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                StudentManagerInterface.class.getSimpleName(), "getImage",
                new Object[]{key}));
        if (msg.getException() != null) {
            throw msg.getException();
        }
    }

    @Override
    public List<Student> findByGroup(String group) throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                StudentManagerInterface.class.getSimpleName(),
                "findByGroup", new Object[]{group}));
        if (msg == null) {
            return null;
        }
        if (msg.getException() != null) {
            throw msg.getException();
        }
        return (List<Student>) msg.getResult();
    }

    @Override
    public Student findById(int studentId) throws Exception {
        RemoteResponseMessage msg = invokeRemoteCall(new RemoteRequestMessage(
                StudentManagerInterface.class.getSimpleName(), "findById",
                new Object[]{studentId}));
        if (msg == null) {
            return null;
        }
        if (msg.getException() != null) {
            throw msg.getException();
        }
        return (Student) msg.getResult();
    }

    private RemoteResponseMessage invokeRemoteCall(
            RemoteRequestMessage remoteRequestMessage) throws Exception {
        Socket clientSocket = new Socket(REMOTE_SERVER_ADDR, REMOTE_SERVER_PORT);
        ObjectOutputStream outToServer = new ObjectOutputStream(
                clientSocket.getOutputStream());
        outToServer.writeObject(remoteRequestMessage);
        // RemoteResponseMessage
        ObjectInputStream inFromServer = new ObjectInputStream(
                clientSocket.getInputStream());
        Object response = inFromServer.readObject();
        clientSocket.close();
        return (RemoteResponseMessage) response;
    }

}
