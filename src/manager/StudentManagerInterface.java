package manager;

import domain.Student;

import java.util.List;

public interface StudentManagerInterface {
    public List<Student> getSortedSubjects(String columnname, String orderVal) throws Exception;
    int createStudent(Student student) throws Exception;

    void updateStudent(Student student) throws Exception;

    void removeStudent(int studentId) throws Exception;

    Student findById(int studentId) throws Exception;

    List<Student> getAllStudents() throws Exception;

    public void getImage(int key) throws Exception;
    public List<Student> findByGroup(String group) throws Exception;
}
