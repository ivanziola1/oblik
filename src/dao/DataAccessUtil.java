package dao;

import java.sql.*;
import java.util.Properties;

public class DataAccessUtil {

    private static final String DRIVER_CLASS = "com.mysql.jdbc.Driver";
    private static final String CONNECTION_URL = "jdbc:mysql://127.0.0.1:3306/oblik";
    private static final String CONNECTION_USERNAME = "root";
    private static final String CONNECTION_PASSWORD = "";

    public static Connection createConnection() throws SQLException {
        //��������� ����� ����� Properties ��� �������� �������� � java
        Properties p = new Properties();
        p.setProperty("user", CONNECTION_USERNAME);
        p.setProperty("password", CONNECTION_PASSWORD);
        p.setProperty("useUnicode", "true");
        p.setProperty("characterEncoding", "utf8");
        try {
            Class.forName(DRIVER_CLASS);

        } catch (Exception e) {
            System.err.println("Driver class is not found, cause:"
                    + e.getMessage());
        }
        return DriverManager.getConnection(CONNECTION_URL, p);
    }

    public static void close(Connection c) {
        try {
            if (c != null && !c.isClosed()) {
                c.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void close(ResultSet rs) {
        try {
            if (rs != null && !rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void close(Connection c, ResultSet rs) {
        close(c);
        close(rs);
    }

    public static int getNewRowKey(PreparedStatement statement)
            throws Exception {
        ResultSet rs = statement.getGeneratedKeys();
        if (rs.next()) {
            return rs.getInt(1);
        }
        return -1;
    }

}
