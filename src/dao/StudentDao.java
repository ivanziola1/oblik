package dao;

import domain.Student;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class StudentDao {
    File image;
    private static final String INSERT_QUERY = "INSERT INTO `oblik`.`students`(`birthday_date`,"
            + "`course`,`curator`,`firstname`,`group`,`lastname`,`middlename`,`photo`,`fileName`,`record_book`,"
            + "`sex`,`studying_type`,`tuition`)"
            + "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?);";

    private static final String UPDATE_QUERY = "UPDATE `oblik`.`students` "
            + " SET `birthday_date` = ?, `course` = ?, `curator` = ?, `firstname` = ?,`group`= ?, `lastname` = ?, "
            + "`middlename` = ?, `photo` = ?, `fileName`= ?, `record_book` = ?, `sex` = ?, `studying_type` = ?, "
            + "`tuition` = ?  where `id` = ?;";

    private static final String DELETE_QUERY = "DELETE FROM `oblik`.`students` where `id` = ?;";

    private static final String SELECT_QUERY = "SELECT `students`.`birthday_date`, `students`.`course`,"
            + "`students`.`curator`, `students`.`firstname`,`students`.`group`,`students`.`id`,`students`.`lastname`, "
            + "`students`.`middlename`, `students`.`photo`,`students`.`fileName`, `students`.`record_book`, `students`.`sex`, "
            + "`students`.`studying_type`, `students`.`tuition` FROM `oblik`.`students`"
            + "where `id` = ?;";
    private static final String SELECT_QUERY_BYGROUP = "SELECT `students`.`birthday_date`, `students`.`course`,"
            + "`students`.`curator`, `students`.`firstname`,`students`.`group`,`students`.`id`,`students`.`lastname`, "
            + "`students`.`middlename`, `students`.`photo`,`students`.`fileName`, `students`.`record_book`, `students`.`sex`, "
            + "`students`.`studying_type`, `students`.`tuition` FROM `oblik`.`students`"
            + "where `group` like ?";

    private static final String SELECT_ALL_QUERY = "SELECT `students`.`birthday_date`, `students`.`course`,"
            + "`students`.`curator`, `students`.`firstname`,`students`.`group`,`students`.`id`,`students`.`lastname`, "
            + "`students`.`middlename`, `students`.`photo`,`students`.`fileName`, `students`.`record_book`, `students`.`sex`, "
            + "`students`.`studying_type`, `students`.`tuition` FROM `oblik`.`students`;";

    public int insertStudent(Student student) throws Exception {

        Connection connection = DataAccessUtil.createConnection();
        PreparedStatement statement = connection.prepareStatement(INSERT_QUERY,
                Statement.RETURN_GENERATED_KEYS);

        try {
            statement.setString(1, student.getBirthdayDate());
            statement.setInt(2, student.getCourse());
            statement.setString(3, student.getCurator());
            statement.setString(4, student.getFirstname());
            statement.setString(5, student.getGroup());
            statement.setString(6, student.getLastname());
            statement.setString(7, student.getMiddlename());
            String s = student.getFileName();
            if (s != "" && s != null) {
                image = new File(s);
            } else image = null;
            FileInputStream fIS = new FileInputStream(image);
            statement.setBinaryStream(8, fIS, (int) image.length());
            statement.setString(9, s);
            statement.setString(10, student.getRecordBook());
            statement.setString(11, student.getSex());
            statement.setString(12, student.getStudyingType());
            statement.setString(13, student.getTuition());
            statement.executeUpdate();

            return DataAccessUtil.getNewRowKey(statement);
        } finally {
            DataAccessUtil.close(connection);
        }
    }

    public void updateStudent(Student student) throws Exception {
        Connection connection = DataAccessUtil.createConnection();
        PreparedStatement statement = connection.prepareStatement(UPDATE_QUERY);

        try {
            statement.setString(1, student.getBirthdayDate());
            statement.setInt(2, student.getCourse());
            statement.setString(3, student.getCurator());
            statement.setString(4, student.getFirstname());
            statement.setString(5, student.getGroup());
            statement.setString(6, student.getLastname());
            statement.setString(7, student.getMiddlename());
            String s = student.getFileName();
            if (s != "" && s != null) {
                File image = new File(s);
                FileInputStream fIS = new FileInputStream(image);
                statement.setBinaryStream(8, fIS, (int) image.length());
            }
            statement.setString(9, s);
            statement.setString(10, student.getRecordBook());
            statement.setString(11, student.getSex());
            statement.setString(12, student.getStudyingType());
            statement.setString(13, student.getTuition());
            statement.setInt(14, student.getId());

            statement.executeUpdate();
        } finally {
            DataAccessUtil.close(connection);
        }
    }

    public void deleteStudent(int studentId) throws Exception {
        Connection connection = DataAccessUtil.createConnection();
        PreparedStatement statement = connection.prepareStatement(DELETE_QUERY);

        try {
            statement.setInt(1, studentId);
            statement.executeUpdate();
        } finally {
            DataAccessUtil.close(connection);
        }
    }

    public Student findById(int studentId) throws Exception {
        Connection connection = DataAccessUtil.createConnection();
        PreparedStatement statement = connection.prepareStatement(SELECT_QUERY);

        try {
            statement.setInt(1, studentId);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return getStudentFromRow(rs);
            }
            return null;
        } finally {
            DataAccessUtil.close(connection);
        }
    }

    public List<Student> findAll() throws Exception {
        Connection connection = DataAccessUtil.createConnection();
        PreparedStatement statement = connection
                .prepareStatement(SELECT_ALL_QUERY);

        try {
            ResultSet rs = statement.executeQuery();
            List<Student> result = new ArrayList<Student>();
            while (rs.next()) {
                result.add(getStudentFromRow(rs));
            }
            return result;
        } finally {
            DataAccessUtil.close(connection);
        }
    }

    public List<Student> getSortedSubjects(String columnname, String orderVal) throws Exception {
        Connection connection = DataAccessUtil.createConnection();
        PreparedStatement statement = connection
                .prepareStatement("SELECT birthday_date, course,curator, firstname,`group`,id,lastname,middlename,photo, fileName, record_book, sex,studying_type, tuition FROM students order by " + columnname + " " + orderVal);

        try {
            ResultSet rs = statement.executeQuery();
            List<Student> result = new ArrayList<Student>();
            while (rs.next()) {
                result.add(getStudentFromRow(rs));
            }
            return result;
        } finally {
            DataAccessUtil.close(connection);
        }
    }

    public List<Student> findByGroup(String group) throws Exception {
        Connection connection = DataAccessUtil.createConnection();
        PreparedStatement statement = connection
                .prepareStatement(SELECT_QUERY_BYGROUP);

        try {
            statement.setString(1, "%" + group + "%");
            ResultSet rs = statement.executeQuery();
            List<Student> result = new ArrayList<Student>();
            while (rs.next()) {
                result.add(getStudentFromRow(rs));
            }
            return result;
        } finally {
            DataAccessUtil.close(connection);
        }
    }

    private Student getStudentFromRow(ResultSet rs) throws Exception {
        Student student = new Student();
        // student.setId(id);
        student.setBirthdayDate(rs.getString(1));
        student.setCourse(rs.getInt(2));
        student.setCurator(rs.getString(3));
        student.setFirstname(rs.getString(4));
        student.setGroup(rs.getString(5));
        student.setId(rs.getInt(6));
        student.setLastname(rs.getString(7));
        student.setMiddlename(rs.getString(8));
        student.setFileName(rs.getString(10));
        student.setRecordBook(rs.getString(11));
        student.setSex(rs.getString(12));
        student.setStudyingType(rs.getString(13));
        student.setTuition(rs.getString(14));

        return student;
    }

    public void getImage(int key) throws Exception {
        Connection connection = DataAccessUtil.createConnection();
        String readImage = ("select photo from students where id = " + key);
        PreparedStatement statement = connection.prepareStatement(readImage);
        ResultSet rs = statement.executeQuery();

        while (rs.next()) {

            FileOutputStream FoS = new FileOutputStream("buf.jpg");
            InputStream is = rs.getBinaryStream(1);
            byte[] buf = new byte[3000];
            int read = 0;
            while ((read = is.read(buf)) > 0) {
                FoS.write(buf, 0, read);
            }
            FoS.close();
            is.close();
        }
    }

}