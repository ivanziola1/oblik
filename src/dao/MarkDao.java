package dao;

import domain.Mark;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class MarkDao {
    private static final String INSERT_QUERY = "INSERT INTO `oblik`.`marks` (`classes_type`, `date`, `mark`,"
            + "`retake`, `student_id`, `subject_id`)" + "values (?,?,?,?,?,?)";
    private static final String UPDATE_QUERY = "UPDATE `oblik`.`marks` "
            + "SET `classes_type` = ?, `date` = ?, `mark` = ?, `retake` = ?, `student_id` = ?,`subject_id` = ? "
            + "WHERE `id` = ?; ";
    private static final String DELETE_QUERY = "DELETE FROM `oblik`.`marks`"
            + "WHERE `id` = ?;";
    private static final String SELECT_QUERY = "SELECT `marks`.`classes_type`,`marks`.`date`,`marks`.`id`,"
            + "`marks`.`mark`, `marks`.`retake`, `marks`.`student_id`, `marks`.`subject_id` "
            + "FROM `oblik`.`marks` where `oblik`.`id`=?";
    private static final String SELECT_FOR_STUDENT_QUERY = "SELECT classes_type, date,id,mark, retake,student_id,subject_id "
            + "FROM marks where id = ";
    private static final String SELECT_FOR_SUBJECT_QUERY = "SELECT `marks`.`classes_type`,`marks`.`date`,`marks`.`id`,"
            + "`marks`.`mark`, `marks`.`retake`, `marks`.`student_id`, `marks`.`subject_id` "
            + "FROM `oblik`.`marks` where `oblik`.`subject_id`=?";
    private static final String SELECT_ALL_QUERY = "SELECT `marks`.`classes_type`,`marks`.`date`,`marks`.`id`,"
            + "`marks`.`mark`, `marks`.`retake`, `marks`.`student_id`, `marks`.`subject_id` "
            + "FROM `oblik`.`marks`;";

    public int insertMark(Mark mark) throws Exception {

        Connection connection = DataAccessUtil.createConnection();
        PreparedStatement statement = connection.prepareStatement(INSERT_QUERY,
                Statement.RETURN_GENERATED_KEYS);

        try {
            statement.setString(1, mark.getClassesType());
            statement.setString(2, mark.getDate());
            statement.setInt(3, mark.getMark());
            statement.setInt(4, mark.getRetake());
            statement.setInt(5, mark.getStudentId());
            statement.setInt(6, mark.getSubjectId());
            statement.executeUpdate();

            return DataAccessUtil.getNewRowKey(statement);
        } finally {
            DataAccessUtil.close(connection);
        }
    }

    public void updateMark(Mark mark) throws Exception {

        Connection connection = DataAccessUtil.createConnection();
        PreparedStatement statement = connection.prepareStatement(UPDATE_QUERY);

        try {
            statement.setString(1, mark.getClassesType());
            statement.setString(2, mark.getDate());
            statement.setInt(3, mark.getMark());
            statement.setInt(4, mark.getRetake());
            statement.setInt(5, mark.getStudentId());
            statement.setInt(6, mark.getSubjectId());
            statement.setInt(7, mark.getId());
            statement.executeUpdate();
        } finally {
            DataAccessUtil.close(connection);
        }
    }

    public void deleteMark(int markId) throws Exception {
        Connection connection = DataAccessUtil.createConnection();
        PreparedStatement statement = connection.prepareStatement(DELETE_QUERY);
        try {
            statement.setInt(1, markId);
            statement.executeUpdate();
        } finally {
            DataAccessUtil.close(connection);
        }
    }

    public Mark findById(int marktId) throws Exception {
        Connection connection = DataAccessUtil.createConnection();
        PreparedStatement statement = connection.prepareStatement(SELECT_QUERY);

        try {
            statement.setInt(1, marktId);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return getMarkFromRow(rs);
            }
            return null;
        } finally {
            DataAccessUtil.close(connection);
        }
    }

    private Mark getMarkFromRow(ResultSet rs) throws Exception {
        Mark mark = new Mark();
        mark.setClassesType(rs.getString(1));
        mark.setDate(rs.getString(2));
        mark.setId(rs.getInt(3));
        mark.setMark(rs.getInt(4));
        mark.setRetake(rs.getInt(5));
        mark.setStudentId(rs.getInt(6));
        mark.setSubjectId(rs.getInt(7));
        return mark;
    }

    public List<Mark> findAll() throws Exception {
        Connection connection = DataAccessUtil.createConnection();
        PreparedStatement statement = connection
                .prepareStatement(SELECT_ALL_QUERY);

        try {
            ResultSet rs = statement.executeQuery();
            List<Mark> result = new ArrayList<Mark>();
            while (rs.next()) {
                result.add(getMarkFromRow(rs));
            }
            return result;
        } finally {
            DataAccessUtil.close(connection);
        }
    }

    public List<Mark> getSortedMarks(String columnname, String orderVal) throws Exception {
        Connection connection = DataAccessUtil.createConnection();
        Statement statement = connection
                .createStatement();

        try {
            ResultSet rs = statement.executeQuery("SELECT classes_type, date,id, mark, retake,student_id," +
                    "subject_id FROM marks group by "+columnname+" " +orderVal);
            List<Mark> result = new ArrayList<Mark>();
            while (rs.next()) {
                result.add(getMarkFromRow(rs));
            }
            return result;
        } finally {
            DataAccessUtil.close(connection);
        }
    }
    public List<Mark> findAllSTudentMarks(int studentId) throws Exception {
        Connection connection = DataAccessUtil.createConnection();
        PreparedStatement statement = connection
                .prepareStatement(SELECT_FOR_STUDENT_QUERY);

        try {
            statement.setInt(1, studentId);
            ResultSet rs = statement.executeQuery();
            List<Mark> result = new ArrayList<Mark>();
            while (rs.next()) {
                result.add(getMarkFromRow(rs));
            }
            return result;
        } finally {
            DataAccessUtil.close(connection);
        }
    }

    public List<Mark> findAllSubjectMarks(int subjectId) throws Exception {
        Connection connection = DataAccessUtil.createConnection();
        PreparedStatement statement = connection
                .prepareStatement(SELECT_FOR_SUBJECT_QUERY);

        try {
            statement.setInt(1, subjectId);
            ResultSet rs = statement.executeQuery();
            List<Mark> result = new ArrayList<Mark>();
            while (rs.next()) {
                result.add(getMarkFromRow(rs));
            }
            return result;
        } finally {
            DataAccessUtil.close(connection);
        }
    }
}
