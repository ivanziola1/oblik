package dao;

import domain.Subject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SubjectDao {
    String columnname;
    private static final String INSERT_QUERY = "INSERT INTO `oblik`.`subjects`(`consultation`, `ECTS`,"
            + "`form_of_control`,`hours`,`laboratory_work`,`lectures`,`practical_training`,`subject_name`,"
            + "`teacher_name`)" + "VALUES(?,?,?,?,?,?,?,?,?);";
    private static final String UPDATE_QUERY = "UPDATE `oblik`.`subjects` "
            + "SET `consultation` = ?, `ECTS` = ?,`form_of_control` = ?,`hours` = ?,`laboratory_work` = ?,"
            + "`lectures` = ?, `practical_training` = ?,`subject_name` = ?,`teacher_name` = ?"
            + "WHERE `id`=?;";
    private static final String DELETE_QUERY = "DELETE FROM `oblik`.`subjects` "
            + "WHERE `id`=?;";
    private static final String SELECT_QUERY = "SELECT consultation, ECTS,"
            + " form_of_control, hours, id, laboratory_work,"
            + " lectures, practical_training, subject_name,"
            + "`subjects`.`teacher_name` FROM subjects "
            + " WHERE id =?;";
    private static final String SELECT_ALL_QUERY = "SELECT\n" +
            "`subjects`.`consultation`,\n" +
            "`subjects`.`ECTS`,\n" +
            "`subjects`.`form_of_control`,\n" +
            "`subjects`.`hours`,\n" +
            "`subjects`.`id`,\n" +
            "`subjects`.`laboratory_work`,\n" +
            "`subjects`.`lectures`,\n" +
            "`subjects`.`practical_training`,\n" +
            "`subjects`.`subject_name`,\n" +
            "`subjects`.`teacher_name`\n" +
            "FROM `oblik`.`subjects`;";

    public int insertSubject(Subject subject) throws Exception {

        Connection connection = DataAccessUtil.createConnection();
        PreparedStatement statement = connection.prepareStatement(INSERT_QUERY,
                Statement.RETURN_GENERATED_KEYS);

        try {
            statement.setInt(1, subject.getConsultation());
            statement.setFloat(2, subject.getEcts());
            statement.setString(3, subject.getFormOfControl());
            statement.setInt(4, subject.getHours());
            statement.setInt(5, subject.getLaboratoryWork());
            statement.setInt(6, subject.getLectures());
            statement.setInt(7, subject.getPracticalTraining());
            statement.setString(8, subject.getSubjectName());
            statement.setString(9, subject.getTeacherName());

            statement.executeUpdate();

            return DataAccessUtil.getNewRowKey(statement);
        } finally {
            DataAccessUtil.close(connection);
        }
    }

    public void updateSubject(Subject subject) throws Exception {

        Connection connection = DataAccessUtil.createConnection();
        PreparedStatement statement = connection.prepareStatement(UPDATE_QUERY);

        try {
            statement.setInt(1, subject.getConsultation());
            statement.setFloat(2, subject.getEcts());
            statement.setString(3, subject.getFormOfControl());
            statement.setInt(4, subject.getHours());
            statement.setInt(5, subject.getLaboratoryWork());
            statement.setInt(6, subject.getLectures());
            statement.setInt(7, subject.getPracticalTraining());
            statement.setString(8, subject.getSubjectName());
            statement.setString(9, subject.getTeacherName());
            statement.setInt(10, subject.getId());
            statement.executeUpdate();
        } finally {
            DataAccessUtil.close(connection);
        }
    }

    public void deleteSubject(int subjectId) throws Exception {
        Connection connection = DataAccessUtil.createConnection();
        PreparedStatement statement = connection.prepareStatement(DELETE_QUERY);

        try {
            statement.setInt(1, subjectId);
            statement.executeUpdate();
        } finally {
            DataAccessUtil.close(connection);
        }

    }

    public Subject findById(int subjectId) throws Exception {
        Connection connection = DataAccessUtil.createConnection();
        PreparedStatement statement = connection.prepareStatement(SELECT_QUERY);

        try {
            statement.setInt(1, subjectId);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return getSubjectFromRow(rs);
            }
            return null;
        } finally {
            DataAccessUtil.close(connection);
        }
    }

    private Subject getSubjectFromRow(ResultSet rs) throws Exception {
        Subject subject = new Subject();
        subject.setConsultation(rs.getInt(1));
        subject.setEcts(rs.getFloat(2));
        subject.setFormOfControl(rs.getString(3));
        subject.setHours(rs.getInt(4));
        subject.setId(rs.getInt(5));
        subject.setLaboratoryWork(rs.getInt(6));
        subject.setLectures(rs.getInt(7));
        subject.setPracticalTraining(rs.getInt(8));
        subject.setSubjectName(rs.getString(9));
        subject.setTeacherName(rs.getString(10));
        return subject;
    }

    public List<Subject> findAll() throws Exception {
        Connection connection = DataAccessUtil.createConnection();
        PreparedStatement statement = connection
                .prepareStatement(SELECT_ALL_QUERY);

        try {
            ResultSet rs = statement.executeQuery();
            List<Subject> result = new ArrayList<Subject>();
            while (rs.next()) {
                result.add(getSubjectFromRow(rs));
            }
            return result;
        } finally {
            DataAccessUtil.close(connection);
        }
    }

    public List<Subject> getSortedSubjects(String columnname, String orderVal) throws Exception {
        Connection connection = DataAccessUtil.createConnection();
        PreparedStatement statement = connection
                .prepareStatement("SELECT consultation, ECTS, form_of_control, hours, id, laboratory_work, lectures,practical_training,subject_name, teacher_name FROM subjects order by "+columnname+" "+orderVal);

        try {
            ResultSet rs = statement.executeQuery();
            List<Subject> result = new ArrayList<Subject>();
            while (rs.next()) {
                result.add(getSubjectFromRow(rs));
            }
            return result;
        } finally {
            DataAccessUtil.close(connection);
        }
    }
}
