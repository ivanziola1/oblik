package dao;

import domain.Scholar;
import domain.StudentRecord;
import domain.TeacherRecord;
import sun.awt.resources.awt_es;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 14.04.15.
 */
public class RequestDao {
    private static final String TEACHER_BOOK_QUERY = "SELECT students.lastname, students.firstname, " +
            "students.middlename, subjects.id, marks.mark,marks.date, marks.retake\n" +
            "FROM oblik.subjects,oblik.students,oblik.marks\n" +
            "where students.id = marks.student_id and subjects.id = marks.subject_id and subjects.id = ?";
    private static final String STUDENTS_AVG_MARKS = "SELECT subjects.subject_name, students.lastname," +
            " students.firstname, students.group, students.record_book, avg(marks.retake) as 'оцінка'" +
            " FROM oblik.students, oblik.marks, oblik.subjects \n" +
            " where students.id = marks.student_id and subjects.id = marks.subject_id \n" +
            " group by subjects.id, students.id";
    private static final String SCHOLLARS_QUERY = "SELECT  students.lastname, students.firstname, students.record_book," +
            " avg(marks.retake),case \n" +
            " when avg(marks.retake)> 0 and avg(marks.retake)<7 then 0\n" +
            " when avg(marks.retake)>=7 and avg(marks.retake)<10 then 500\n" +
            " when avg(marks.retake)>=10 and avg(marks.retake)<=12 then 650\n" +
            " else 0\n" +
            " end as  mark \n" +
            " FROM oblik.students, oblik.marks, oblik.subjects \n" +
            "where students.id = marks.student_id and subjects.id = marks.subject_id \n" +
            "group by students.id;";

    public List<TeacherRecord> findAllSubjectMarks(int subjectId) throws Exception {
        Connection connection = DataAccessUtil.createConnection();
        PreparedStatement statement = connection
                .prepareStatement(TEACHER_BOOK_QUERY);

        try {
            statement.setInt(1, subjectId);
            ResultSet rs = statement.executeQuery();
            List<TeacherRecord> result = new ArrayList<TeacherRecord>();
            while (rs.next()) {
                result.add(getTeacherRecordRow(rs));
            }
            return result;
        } finally {
            DataAccessUtil.close(connection);
        }
    }

    public List<StudentRecord> findAllStudentsAVGMarks() throws Exception {
        Connection connection = DataAccessUtil.createConnection();
        PreparedStatement statement = connection
                .prepareStatement(STUDENTS_AVG_MARKS);

        try {
            ResultSet rs = statement.executeQuery();
            List<StudentRecord> result = new ArrayList<StudentRecord>();
            while (rs.next()) {
                result.add(getStudentAVGMarkRecordRow(rs));
            }
            return result;
        } finally {
            DataAccessUtil.close(connection);
        }
    }
    public List<Scholar> findAllSchollars() throws Exception {
        Connection connection = DataAccessUtil.createConnection();
        PreparedStatement statement = connection
                .prepareStatement(SCHOLLARS_QUERY);

        try {
            ResultSet rs = statement.executeQuery();
            List<Scholar> result = new ArrayList<Scholar>();
            while (rs.next()) {
                result.add(getScholardRow(rs));
            }
            return result;
        } finally {
            DataAccessUtil.close(connection);
        }
    }

    private Scholar getScholardRow(ResultSet rs) throws Exception{
        Scholar s = new Scholar();
        s.setLastname(rs.getString(1));
        s.setFirstname(rs.getString(2));
        s.setRecordBook(rs.getString(3));
        s.setAvgMark(rs.getFloat(4));
        s.setScholarship(rs.getInt(5));

        return s;
    }

    private StudentRecord getStudentAVGMarkRecordRow(ResultSet rs) throws Exception {
        StudentRecord rec = new StudentRecord();
        rec.setSubjectName(rs.getString(1));
        rec.setLastname(rs.getString(2));
        rec.setFirstname(rs.getString(3));
        rec.setGroup(rs.getString(4));
        rec.setRecordBook(rs.getString(5));
        rec.setMark(rs.getFloat(6));
        return rec;
    }

    private TeacherRecord getTeacherRecordRow(ResultSet rs) throws Exception {
        TeacherRecord record = new TeacherRecord();
        record.setLastname(rs.getString(1));
        record.setFirstname(rs.getString(2));
        record.setMiddlename(rs.getString(3));
        record.setSubjectId(rs.getInt(4));
        record.setMark(rs.getInt(5));
        record.setDate(rs.getString(6));
        record.setRetake(rs.getInt(7));
        return record;
    }
}
