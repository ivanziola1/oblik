package gui;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import domain.Scholar;
import manager.RemoteRequestManagerProxy;
import manager.RequestManagerInterface;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by admin on 29.04.15.
 */
public class SchollarsForm extends JFrame implements ActionListener {
    RequestManagerInterface requestManager;
    private JTable mTable;
    private SchollarsTableModel schollarsTableModel;
    private JButton createReport;
    String windows = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

    public SchollarsForm() {
        try {
            UIManager.setLookAndFeel(windows);
        } catch (Exception e2) {
            //
            e2.printStackTrace();
        }
        requestManager = new RemoteRequestManagerProxy();
        schollarsTableModel = getTableModel();
        mTable = new JTable(schollarsTableModel);
        mTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        mTable.setPreferredScrollableViewportSize(new Dimension(450, 370));
        mTable.setRowHeight(20);
        setSize(470, 400);
        JScrollPane scrollPane = new JScrollPane(mTable);
        scrollPane.setOpaque(false);
        scrollPane.getViewport().setOpaque(false);

        JPanel mainPanel = new JPanel();
        mainPanel.add(scrollPane);
        setResizable(false);
        JToolBar tools = new JToolBar();
        getContentPane().add(mainPanel, BorderLayout.CENTER);
        getContentPane().add(tools, BorderLayout.NORTH);
        createReport = new JButton("Сформувати звіт");
        getContentPane().add(createReport, BorderLayout.SOUTH);
        createReport.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                createReport();
            }
        });

    }

    private SchollarsTableModel getTableModel() {
        try {
            final List<Scholar> records = requestManager.getAllSchollars();
            return new SchollarsTableModel(records);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Помилка при заповненні: " + e.getMessage());
        }
        return new SchollarsTableModel(new ArrayList<Scholar>(0));
    }

    private void createReport() {
        Thread t = new Thread() {
            public void run() {
                try {

                    Document document = new Document();
                    DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
                    Date date = new Date();

                    PdfWriter.getInstance(document, new FileOutputStream("reports\\Schollars_" + dateFormat.format(date) + ".pdf"));
                    document.open();
                    BaseFont bf = BaseFont.createFont("font\\arial.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                    com.itextpdf.text.Font font = new com.itextpdf.text.Font(bf);
                    document.setMargins(0, 0, 0, 0);
                    PdfPTable tab = new PdfPTable(getTableModel().getColumnCount());
                    for (int i = 0; i < getTableModel().getColumnCount(); i++)
                        tab.addCell(new Paragraph(getTableModel().getColumnName(i), font));
                    for (int i = 0; i < getTableModel().getRowCount(); i++) {
                        for (int j = 0; j < getTableModel().getColumnCount(); j++) {
                            Object obj1 = GetData(mTable, i, j);
                            String value1 = obj1.toString();
                            tab.addCell(new Paragraph(value1, font));
                        }
                    }
                    document.add(tab);
                    document.add(new Paragraph(("Дата формування звіту:                                             " +
                            "                            " + getDate()), font));
                    document.close();
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            JOptionPane.showMessageDialog(SchollarsForm.this, "Звіт створено");
                        }
                    });
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(SchollarsForm.this, "Помилка: " + e.getMessage());
                }

            }
        };
        t.start();
    }

    private String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        return dateFormat.format(new Date());
    }

    public Object GetData(JTable table, int row_index, int col_index) {
        return table.getModel().getValueAt(row_index, col_index);
    }

    /**
     * Invoked when an action occurs.
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
