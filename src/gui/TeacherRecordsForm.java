package gui;

import domain.Mark;
import domain.Subject;
import domain.TeacherRecord;
import manager.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 14.04.15.
 */
public class TeacherRecordsForm extends JFrame implements ActionListener {
    JLabel label1;
    JComboBox subjectsList;
    private JTable mTable;
    SubjectManagerInterface subjectManager;
    private TeacherRecordsTableModel recordsTableModel;
    private RequestManagerInterface requestManager;
    private MarkManagerInterface markManager;
    String windows = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
    List<Subject> subjects;
    JScrollPane scrollPane;
    JPanel mainPanel;
    List<Mark> marks;

    /**
     * Invoked when an action occurs.
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

    }

    public TeacherRecordsForm() {
        try {
            UIManager.setLookAndFeel(windows);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        requestManager = new RemoteRequestManagerProxy();
        subjectManager = new RemoteSubjectManagerProxy();
        markManager = new RemoteMarkManagerProxy();
        try {
            marks = markManager.getSortedMarks("date","asc");
        } catch (Exception e) {
            e.printStackTrace();
        }
        label1 = new JLabel(" Виберіть предмет: ");
        setTitle("Оцінки студентів");

        try {
            subjects = subjectManager.getSortedSubjects("id","asc");
        } catch (Exception e) {
            e.printStackTrace();
        }
        recordsTableModel = getTableModel(subjects.get(0).getId().intValue());

        mTable = new JTable(recordsTableModel);
        mTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        mTable.setPreferredScrollableViewportSize(new Dimension(450, 400));
        mTable.setRowHeight(20);

        scrollPane = new JScrollPane(mTable);
        scrollPane.setOpaque(false);
        scrollPane.getViewport().setOpaque(false);

        mainPanel = new JPanel();
        mainPanel.add(scrollPane);
        setSize(470, 450);
        setResizable(false);
        JToolBar tools = new JToolBar();
        getContentPane().add(mainPanel, BorderLayout.CENTER);
        getContentPane().add(tools, BorderLayout.NORTH);


        subjectsList = new JComboBox(subjects.toArray());
        for (Subject sub : subjects) {
            if (sub.getId().intValue() == subjects.get(0).getId().intValue()) {
                subjectsList.setSelectedItem(sub);

                System.out.println(marks.get(1).getSubjectId().intValue());
            }
        }

        subjectsList.setEditable(false);
        tools.add(label1);
        tools.add(subjectsList);
        subjectsList.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Subject s = (Subject) subjectsList.getSelectedItem();
                recordsTableModel = getTableModel(s.getId());
                mTable.setModel(recordsTableModel);
            }
        });
    }


    private TeacherRecordsTableModel getTableModel(int subjectId) {
        try {
            final List<TeacherRecord> records = requestManager.getAllSubjectMarks(subjectId);
            return new TeacherRecordsTableModel(records);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Помилка при заповненні: " + e.getMessage());
        }
        return new TeacherRecordsTableModel(new ArrayList<TeacherRecord>(0));
    }

}
