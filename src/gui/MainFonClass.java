package gui;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class MainFonClass extends JPanel {

    private static final long serialVersionUID = 1L;

    private BufferedImage Fonimage;

    public MainFonClass() {
        try {
            Fonimage = ImageIO.read(new File("img/mainfon.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(Fonimage, 0, 0, getWidth(), getHeight(), null);
    }
}