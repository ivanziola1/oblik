package gui;

import domain.StudentRecord;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * Created by admin on 23.04.15.
 */
public class StudentsAVGMarksModel extends AbstractTableModel {
    private String[] columns = new String[]{"Предмет", "Прізвище", "Ім'я",
            "Група", "Залікова кн.", "Середній бал"};
    List<StudentRecord> records;

    public StudentsAVGMarksModel(List<StudentRecord> records) {
        this.records = records;
    }

    /**
     * Returns the number of rows in the model. A
     * <code>JTable</code> uses this method to determine how many rows it
     * should display.  This method should be quick, as it
     * is called frequently during rendering.
     *
     * @return the number of rows in the model
     * @see #getColumnCount
     */
    @Override
    public int getRowCount() {
        return records.size();
    }

    /**
     * Returns the number of columns in the model. A
     * <code>JTable</code> uses this method to determine how many columns it
     * should create and display by default.
     *
     * @return the number of columns in the model
     * @see #getRowCount
     */
    @Override
    public int getColumnCount() {
        return columns.length;
    }

    /**
     * Returns the value for the cell at <code>columnIndex</code> and
     * <code>rowIndex</code>.
     *
     * @param rowIndex    the row whose value is to be queried
     * @param columnIndex the column whose value is to be queried
     * @return the value Object at the specified cell
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        StudentRecord r = records.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return r.getSubjectName();
            case 1:
                return r.getLastname();
            case 2:
                return r.getFirstname();
            case 3:
                return r.getGroup();
            case 4:
                return r.getRecordBook();
            case 5:
                return Float.toString(r.getMark());
        }
        return "";

    }

    public StudentRecord getRowRecord(int rowIndex) {
        return records.get(rowIndex);
    }

    public String getColumnName(int columnIndex) {
        return columns[columnIndex];
    }

    public void refreshUpdatedTable() {
        fireTableRowsUpdated(0, records.size());
    }
}
