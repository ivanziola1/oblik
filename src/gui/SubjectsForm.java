package gui;

import domain.Subject;
import manager.RemoteSubjectManagerProxy;
import manager.SubjectManagerInterface;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.print.PrinterException;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;

/**
 * Created by admin on 28.04.15.
 */
public class SubjectsForm extends JFrame implements ActionListener {
    String windows = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
    SubjectManagerInterface subjectManager;
    private JTable mTable;
    private SubjectsTableModel subjectsTableModel;
    JButton addSubject, editSubject, bprint, delete;
    private NewSubject newSubject;
    int i1 = 0;
    int i2 = 0;
    int i3 = 0;
    int i4 = 0;
    int i5 = 0;
    int i6 = 0;
    int i7 = 0;
    int i8 = 0;
    JPopupMenu popupMenu = new JPopupMenu();
    int i9 = 0;
    JMenuItem addSubjectt, updateSubject, removeSubject, printSubject, onClose;
    Font font = new Font("Times New Roman", Font.BOLD, 17);
    public SubjectsForm() {
        try {
            UIManager.setLookAndFeel(windows);
        } catch (Exception e2) {
            //
            e2.printStackTrace();
        }
        createMenu();
        subjectManager = new RemoteSubjectManagerProxy();
        setTitle("Предмети");
        subjectsTableModel = getTableModel("subject_name", "asc");
        mTable = new JTable(subjectsTableModel);
        mTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        mTable.setPreferredScrollableViewportSize(new Dimension(650, 200));
        mTable.setRowHeight(20);
        mTable.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    updateSubject();
                }
            }
        });

        JScrollPane scrollPane = new JScrollPane(mTable);
        scrollPane.setOpaque(false);
        scrollPane.getViewport().setOpaque(false);

        JPanel mainPanel = new JPanel();
        mainPanel.add(scrollPane);

        setSize(670, 310);
        setResizable(false);
        JToolBar tools = new JToolBar();
        getContentPane().add(mainPanel, BorderLayout.CENTER);
        getContentPane().add(tools, BorderLayout.SOUTH);
        addSubject = new JButton("Додати предмет");
        addSubject.setFont(font);
        tools.add(addSubject);
        addSubject.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                createSubject();
            }
        });
        editSubject = new JButton("Редагувати");
        editSubject.setFont(font);
        tools.add(editSubject);
        editSubject.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateSubject();
            }
        });
        delete = new JButton("Видалити");
        delete.setFont(font);
        tools.add(delete);
        delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteSubject();
            }
        });
        bprint = new JButton("Надрукувати");
        bprint.setFont(font);
        tools.add(bprint);
        bprint.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printSubject();
            }
        });
        mTable.getTableHeader().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int col = mTable.columnAtPoint(e.getPoint());
                orderSubjects(col);

            }
        });
        JMenuItem cmdKmCreate = new JMenuItem("Додати запис");
        cmdKmCreate.addActionListener(this);
        popupMenu.add(cmdKmCreate);

        cmdKmCreate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                createSubject();
            }
        });
        JMenuItem cmdKmUpdate = new JMenuItem("Редагувати запис");
        cmdKmUpdate.addActionListener(this);
        popupMenu.add(cmdKmUpdate);

        cmdKmUpdate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                updateSubject();
            }
        });

        JMenuItem cmdKmRemove = new JMenuItem("Видалити запис");
        cmdKmRemove.addActionListener(this);
        popupMenu.add(cmdKmRemove);

        cmdKmRemove.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                deleteSubject();
            }
        });
        mTable.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                if (me.isPopupTrigger())
                    popupMenu.show(me.getComponent(), me.getX(), me.getY());
            }

            public void mouseReleased(MouseEvent me) {
                if (me.isPopupTrigger())
                    popupMenu.show(me.getComponent(), me.getX(), me.getY());
            }
        });
        tools.setFloatable(false);
    }

    private void orderSubjects(int col) {
        switch (col) {
            case 0: {
                i1++;
                if (i1 % 2 == 0 && i1 != 0) {
                    subjectsTableModel = getTableModel("subject_name", "asc");
                    mTable.setModel(subjectsTableModel);
                } else if (i1 != 0 && i1 % 2 != 0) {
                    subjectsTableModel = getTableModel("subject_name", "desc");
                    mTable.setModel(subjectsTableModel);

                }

            }
            break;
            case 1: {
                i2++;
                if (i2 % 2 == 0 && i2 != 0) {
                    subjectsTableModel = getTableModel("teacher_name", "asc");
                    mTable.setModel(subjectsTableModel);
                } else if (i2 != 0 && (i2 % 2 != 0)) {
                    subjectsTableModel = getTableModel("teacher_name", "desc");
                    mTable.setModel(subjectsTableModel);
                }

            }
            break;
            case 2: {
                i3++;
                if (i3 % 2 == 0 && i3 != 0) {
                    subjectsTableModel = getTableModel("ECTS", "asc");
                    mTable.setModel(subjectsTableModel);
                } else if (i3 != 0 && (i3 % 2 != 0)) {
                    subjectsTableModel = getTableModel("ECTS", "desc");
                    mTable.setModel(subjectsTableModel);
                }

            }
            break;
            case 3: {
                i4++;
                if (i4 % 2 == 0 && i4 != 0) {
                    subjectsTableModel = getTableModel("hours", "asc");
                    mTable.setModel(subjectsTableModel);
                } else if (i4 != 0) {
                    subjectsTableModel = getTableModel("hours", "desc");
                    mTable.setModel(subjectsTableModel);
                }

            }
            break;
            case 4: {
                i5++;
                if (i5 % 2 == 0 && i5 != 0) {

                    subjectsTableModel = getTableModel("lectures", "asc");
                    mTable.setModel(subjectsTableModel);
                } else if (i5 != 0) {
                    subjectsTableModel = getTableModel("lectures", "desc");
                    mTable.setModel(subjectsTableModel);
                }

            }
            break;
            case 5: {
                i6++;
                if (i6 % 2 == 0 && i6 != 0) {
                    subjectsTableModel = getTableModel("practical_training", "asc");
                    mTable.setModel(subjectsTableModel);
                } else if (i6 != 0) {
                    subjectsTableModel = getTableModel("practical_training", "desc");
                    mTable.setModel(subjectsTableModel);
                }

            }
            break;
            case 6: {
                i7++;
                if (i7 % 2 == 0 && i7 != 0) {
                    subjectsTableModel = getTableModel("laboratory_work", "asc");
                    mTable.setModel(subjectsTableModel);
                } else if (i7 != 0) {
                    subjectsTableModel = getTableModel("laboratory_work", "desc");
                    mTable.setModel(subjectsTableModel);
                }

            }
            break;
            case 7: {
                i8++;
                if (i8 % 2 == 0 && i8 != 0) {
                    subjectsTableModel = getTableModel("consultation", "asc");
                    mTable.setModel(subjectsTableModel);
                } else if (i8 != 0) {
                    subjectsTableModel = getTableModel("consultation", "desc");
                    mTable.setModel(subjectsTableModel);
                }

            }
            break;
            case 8: {
                i9++;
                if (i9 % 2 == 0 && i9 != 0) {
                    subjectsTableModel = getTableModel("form_of_control", "asc");
                    mTable.setModel(subjectsTableModel);
                } else if (i9 != 0) {
                    subjectsTableModel = getTableModel("form_of_control", "desc");
                    mTable.setModel(subjectsTableModel);
                }

            }
            break;
        }
    }

    private void deleteSubject() {
        int index = mTable.getSelectedRow();
        if (index == -1) {
            JOptionPane.showMessageDialog(this,
                    "Виберіть предмет");
            return;

        }
        if (JOptionPane.showConfirmDialog(SubjectsForm.this,
                "Ви дійсно бажаєте видалити предмет?",
                "Message", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {


            try {
                Subject s = subjectsTableModel.getRowSubject(index);
                if (s != null) {
                    subjectManager.removeSubject(s.getId().intValue());
                    subjectsTableModel.removeRow(index);
                }
            } catch (SQLException e2) {
                JOptionPane.showMessageDialog(SubjectsForm.this, "Неможливо видалити предмет оскільки є повязані записи");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(SubjectsForm.this, e.getMessage());
            }
        }
    }

    private void printSubject() {
        try {
            MessageFormat headerFormat = new MessageFormat("Сторінка {0}");
            MessageFormat footerFormat = new MessageFormat("- {0} -");
            mTable.print(JTable.PrintMode.FIT_WIDTH, headerFormat,
                    footerFormat);
        } catch (PrinterException pe) {
            System.err.println("Неможливо роздрукувати: "
                    + pe.getMessage());
        }
    }

    private void updateSubject() {
        int index = mTable.getSelectedRow();
        if (index == -1) {
            JOptionPane.showMessageDialog(this,
                    "Виберіть предмет");
            return;
        }
        Subject s = subjectsTableModel.getRowSubject(index);
        if (s != null) {
            newSubject = new NewSubject();
            newSubject.setSubject(s);
            newSubject.setVisible(true);

        }
        subjectsTableModel.refreshUpdatedTable();
    }

    private void createSubject() {
        newSubject = new NewSubject();
        newSubject.setSubject(new Subject());
        newSubject.setVisible(true);
        if (newSubject.getSubject() != null && newSubject.getSubject().getId() != null)
            subjectsTableModel.addSubject(newSubject.getSubject());
    }

    private SubjectsTableModel getTableModel(String columname, String ordVal) {
        try {
            final java.util.List<Subject> subjects = subjectManager.getSortedSubjects(columname, ordVal);
            for (Subject sub : subjects)
                System.out.println(sub);
            return new SubjectsTableModel(subjects);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Помилка при заповненні: " + e.getMessage());
        }
        return new SubjectsTableModel((new ArrayList<Subject>(0)));
    }

    private void createMenu() {
        Font fontMenu = new Font(Font.MONOSPACED, Font.PLAIN, 14);

        JMenuBar menuBar = new JMenuBar();

        JMenu mFile = new JMenu("Файл");
        JMenu mRead = new JMenu("Правка");
        addSubjectt = new JMenuItem("Додати");
        addSubjectt.setToolTipText("Додати предмети до БД");
        addSubjectt.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U,
                ActionEvent.CTRL_MASK));
        addSubjectt.addActionListener(this);
        addSubjectt.setFont(fontMenu);
        mRead.add(addSubjectt);
        updateSubject = new JMenuItem("Внести зміни");
        updateSubject.setToolTipText("Внести зміни у вже створений запис");
        updateSubject.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U,
                ActionEvent.CTRL_MASK));
        updateSubject.addActionListener(this);
        updateSubject.setFont(fontMenu);
        mRead.add(updateSubject);

        removeSubject = new JMenuItem("Видалити Предмет");
        removeSubject.setToolTipText("Видалити запис з БД");
        removeSubject.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D,
                ActionEvent.SHIFT_MASK));
        removeSubject.setFont(fontMenu);
        removeSubject.addActionListener(this);
        mRead.add(removeSubject);


        printSubject = new JMenuItem("Вивести на друк");
        printSubject.setToolTipText("Роздрукувати інформацію з таблиці");
        printSubject.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P,
                ActionEvent.CTRL_MASK));
        printSubject.addActionListener(this);
        mFile.add(printSubject);
        printSubject.setFont(fontMenu);
        mFile.addSeparator();


        onClose = new JMenuItem("Закрити");
        onClose.setToolTipText("Закрити таблицю");
        onClose.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4,
                ActionEvent.ALT_MASK));
        onClose.setFont(fontMenu);
        onClose.addActionListener(this);
        mFile.add(onClose);


        menuBar.add(mFile);
        menuBar.add(mRead);

        setJMenuBar(menuBar);
    }

    /**
     * Invoked when an action occurs.
     *
     * @param e
     */
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == addSubjectt) {
            createSubject();
        } else if (e.getSource() == updateSubject) {
            updateSubject();
        } else if (e.getSource() == printSubject) {
            printSubject();

        } else if (e.getSource() == removeSubject) {
            deleteSubject();

        } else if (e.getSource() == onClose) {
            if (JOptionPane.showConfirmDialog(SubjectsForm.this,
                    "Ви впевнені що хочете вийти?",
                    "Увага!", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
                SubjectsForm.this.setVisible(false);
        }
    }
}

