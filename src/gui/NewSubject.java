package gui;

import domain.Subject;
import manager.RemoteSubjectManagerProxy;
import manager.SubjectManagerInterface;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by admin on 28.04.15.
 */
public class NewSubject extends JDialog {
    String windows = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
    SubjectManagerInterface subjectManager;
    private Subject subject;
    JLabel label1, label2, label3, label4, label5, label6, label7, label8, label9;
    private JButton cmdSave, cmdCancel;
    private JTextField subjectNameText, teacherNameText, ectsText, hoursText, lecturesText, practicalTrainingText, laboratoryWorkText, consultationsText;
    private JComboBox formOfCOntrolBox;

    public NewSubject() {
        try {
            UIManager.setLookAndFeel(windows);
        } catch (Exception e2) {
            //
            e2.printStackTrace();
        }
        subjectManager = new RemoteSubjectManagerProxy();
        setTitle("Предмет");
        setModal(true);
        setResizable(false);
        setSize(550, 400);
        cmdSave = new JButton("Зберегти");
        cmdCancel = new JButton("Відмінити");
        teacherNameText = new JTextField(30);
        subjectNameText = new JTextField(30);
        ectsText = new JTextField(5);
        hoursText = new JTextField(5);
        lecturesText = new JTextField(5);
        practicalTrainingText = new JTextField(5);
        laboratoryWorkText = new JTextField(5);
        consultationsText = new JTextField(5);
        formOfCOntrolBox = new JComboBox(new Object[]{"екзамен",
                "залік", "диференційований залік"});
        formOfCOntrolBox.setEditable(false);
        JPanel newSubjectMainPanel = new JPanel();
        final JPanel fieldsPanel = new JPanel(new GridLayout(9, 2, 2, 10));
        final JPanel fieldsPanelBorder = new JPanel(new FlowLayout(
                FlowLayout.CENTER, 2, 2));
        fieldsPanel.setOpaque(false);
        fieldsPanelBorder.setOpaque(false);
        fieldsPanelBorder.add(fieldsPanel);
        label1 = new JLabel("Назва предмету");
        label2 = new JLabel("Викладач");
        label3 = new JLabel("ECTS");
        label4 = new JLabel("Години");
        label5 = new JLabel("Лекційні заняття");
        label6 = new JLabel("Практичні заняття");
        label7 = new JLabel("Лабораторні роботи");
        label8 = new JLabel("Консультації");
        label9 = new JLabel("Форма контролю");
        fieldsPanel.add(label1);
        fieldsPanel.add(subjectNameText);
        fieldsPanel.add(label2);
        fieldsPanel.add(teacherNameText);
        fieldsPanel.add(label3);
        fieldsPanel.add(ectsText);
        fieldsPanel.add(label4);
        fieldsPanel.add(hoursText);
        fieldsPanel.add(label5);
        fieldsPanel.add(lecturesText);
        fieldsPanel.add(label6);
        fieldsPanel.add(practicalTrainingText);
        fieldsPanel.add(label7);
        fieldsPanel.add(laboratoryWorkText);
        fieldsPanel.add(label8);
        fieldsPanel.add(consultationsText);
        fieldsPanel.add(label9);
        fieldsPanel.add(formOfCOntrolBox);
        fieldsPanel.add(label2);
        fieldsPanel.add(teacherNameText);
        final JPanel commandsPanel = new JPanel(new FlowLayout());
        final JPanel commandsPanelBorder = new JPanel(new FlowLayout(
                FlowLayout.CENTER, 0, 0));
        commandsPanel.setOpaque(false);
        commandsPanelBorder.setOpaque(false);
        commandsPanelBorder.add(commandsPanel);
        commandsPanel.add(cmdSave);
        commandsPanel.add(cmdCancel);
        newSubjectMainPanel.add(fieldsPanelBorder, BorderLayout.NORTH);
        newSubjectMainPanel.add(commandsPanelBorder, BorderLayout.SOUTH);
        Container c = getContentPane();
        c.add(newSubjectMainPanel);
        cmdSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                saveSubject();
            }
        });
        cmdCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cancelSave();
            }
        });
    }

    private void cancelSave() {

        this.setVisible(false);

    }

    private void saveSubject() {
        try {

            System.out.println(subject + "ins");
            System.out.println(subject.getId());
            subject.setHours(Integer.valueOf(hoursText.getText()));
            subject.setTeacherName(teacherNameText.getText());
            subject.setSubjectName(subjectNameText.getText());
            subject.setLectures(Integer.valueOf(lecturesText.getText()));
            subject.setLaboratoryWork(Integer.valueOf(laboratoryWorkText.getText()));
            subject.setPracticalTraining(Integer.valueOf(practicalTrainingText.getText()));
            subject.setFormOfControl((String) formOfCOntrolBox.getSelectedItem());
            subject.setEcts(Float.valueOf(ectsText.getText()));
            subject.setConsultation(Integer.valueOf(consultationsText.getText()));

            if (subject.getId() == null) {

                int newId = subjectManager.createSubject(subject);
                subject.setId(newId);

            } else {
                System.out.println(subject);
                System.out.println(subject.getId());
                subjectManager.updateSubject(subject);

            }


        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Помилка при збереженні: " + e.getMessage());
        } finally {

            this.setVisible(false);
        }
    }

    void setSubject(Subject subject) {
        this.subject = subject;
        subjectNameText.setText(subject.getSubjectName());
        teacherNameText.setText(subject.getTeacherName());
        if (subject.getEcts() != null)
            ectsText.setText(subject.getEcts().toString());
        if (subject.getConsultation() != null)
            consultationsText.setText(subject.getConsultation().toString());
        if (subject.getHours() != null)
            hoursText.setText(subject.getHours().toString());
        if (subject.getLaboratoryWork() != null)
            laboratoryWorkText.setText(subject.getLaboratoryWork().toString());
        if (subject.getLectures() != null)
            lecturesText.setText(subject.getLectures().toString());
        formOfCOntrolBox.setSelectedItem(subject.getFormOfControl());
        if (subject.getPracticalTraining() != null)
            practicalTrainingText.setText(subject.getPracticalTraining().toString());
        System.out.println(this.subject + " " + this.subject.getId());
    }

    Subject getSubject() {
        return subject;

    }

}

