package gui;

import domain.Subject;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class SubjectsTableModel extends AbstractTableModel {


    private static final long serialVersionUID = 1L;
    private String[] columns = new String[]{"Назва предмету", "Викладач", "ECTS", "Години", "Лекції", "Практичні заняття",
            "Лабораторні роботи", "Консультації", "Форма контролю"};
    private List<Subject> subjects;

    public SubjectsTableModel(List<Subject> subjects) {
        this.subjects = subjects;
    }


    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public int getRowCount() {
        return subjects.size();
    }

    public Subject getRowSubject(int rowIndex) {
        return subjects.get(rowIndex);
    }

    public String getColumnName(int columnIndex) {
        return columns[columnIndex];
    }

    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    public void addSubject(Subject subject) {
        subjects.add(subject);
        fireTableRowsInserted(0, subjects.size());
    }

    public void removeRow(int rowIndex) {
        subjects.remove(rowIndex);
        fireTableRowsDeleted(rowIndex, rowIndex);
    }

    public void refreshUpdatedTable() {
        fireTableRowsUpdated(0, subjects.size());
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Subject subject = getRowSubject(rowIndex);
        switch (columnIndex) {
            case 0:
                return subject.getSubjectName();
            case 1:
                return subject.getTeacherName();
            case 2:
                return subject.getEcts();
            case 3:
                return subject.getHours();
            case 4:
                return subject.getLectures();
            case 5:
                return subject.getPracticalTraining();
            case 6:
                return subject.getLaboratoryWork();
            case 7:
                return subject.getConsultation();
            case 8:
                return subject.getFormOfControl();
        }
        return "";
    }
}
