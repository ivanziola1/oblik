package gui;

import domain.Student;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class StudentsTableModel extends AbstractTableModel {
    /**
     * default
     */
    private static final long serialVersionUID = 1L;
    private String[] columns = new String[]{"Прізвище", "Ім'я",
            "По-батькові", "Дата народження", "Курс", "Група", "Куратор",
            "Залікова книжка", "Стать", "Форма", "Вид навчання"};
    private List<Student> students;

    public StudentsTableModel(List<Student> students) {
        this.students = students;
    }

    public void addStudent(Student student) {
        students.add(student);
        fireTableRowsInserted(0, students.size());
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public int getRowCount() {
        return students.size();
    }

    public Student getRowStudent(int rowIndex) {
        return students.get(rowIndex);
    }

    public String getColumnName(int columnIndex) {
        return columns[columnIndex];
    }
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (students.isEmpty()) {
            return Object.class;
        }
        return getValueAt(0, columnIndex).getClass();
    }


    public void removeRow(int rowIndex) {
        students.remove(rowIndex);
        fireTableRowsDeleted(rowIndex, rowIndex);
    }

    public void refreshUpdatedTable() {
        fireTableRowsUpdated(0, students.size());
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Student student = students.get(rowIndex);
        switch (columnIndex) {

            case 0:
                return student.getLastname();
            case 1:
                return student.getFirstname();
            case 2:
                return student.getMiddlename();
            case 3:
                return student.getBirthdayDate();
            case 4:
                return Integer.toString(student.getCourse());
            case 5:
                return student.getGroup();

            case 6:
                return student.getCurator();

            case 7:
                return student.getRecordBook();
            case 8:
                return student.getSex();
            case 9:
                return student.getStudyingType();
            case 10:
                return student.getTuition();

        }
        return "";
    }
}
