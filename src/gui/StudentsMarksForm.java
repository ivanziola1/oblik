package gui;

import domain.Mark;
import domain.Student;
import manager.MarkManagerInterface;
import manager.RemoteMarkManagerProxy;
import manager.RemoteStudentManagerProxy;
import manager.StudentManagerInterface;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.print.PrinterException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class StudentsMarksForm extends JFrame implements ActionListener {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    JButton bprint, bupdate, bremove, cmdClose;
    private JTable mTable;
    private MarksTableModel marksTableModel;
    private NewMark newMark;
    private MarkManagerInterface markManager;
    private StudentManagerInterface studentManager;
    String windows = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
    JPopupMenu popupMenu = new JPopupMenu();
    JMenuItem  updateMark, removeMark, printMark, onClose;
    int i1 = 0;
    int i2 = 0;
    int i3 = 0;
    int i4 = 0;
    int i5 = 0;
    int i6 = 0;
    int i7 = 0;
    public StudentsMarksForm() {
        try {
            UIManager.setLookAndFeel(windows);
        } catch (Exception e2) {
            //
            e2.printStackTrace();
        }
        createMenu();
        markManager = new RemoteMarkManagerProxy();
        studentManager = new RemoteStudentManagerProxy();
        newMark = new NewMark();
        setTitle("Оцінки студента");

        marksTableModel = getTableModel("date", "asc");
        mTable = new JTable(marksTableModel);
        mTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        mTable.setPreferredScrollableViewportSize(new Dimension(940, 200));
        mTable.setRowHeight(20);
        mTable.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    updateMark();
                }
            }
        });
        JScrollPane scrollPane = new JScrollPane(mTable);
        scrollPane.setOpaque(false);
        scrollPane.getViewport().setOpaque(false);
    setResizable(false);
        JMenuItem cmdKmUpdate = new JMenuItem("Редагувати запис");
        cmdKmUpdate.addActionListener(this);
        popupMenu.add(cmdKmUpdate);

        cmdKmUpdate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                updateMark();
            }
        });

        JMenuItem cmdKmRemove = new JMenuItem("Видалити запис");
        cmdKmRemove.addActionListener(this);
        popupMenu.add(cmdKmRemove);

        cmdKmRemove.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                removeMark();
            }
        });

        cmdClose = new JButton("Закрити");
        JPanel mainPanel = new JPanel();
        mainPanel.add(scrollPane);
        Font font = new Font("Times New Roman", Font.BOLD, 17);
        cmdClose = new JButton("Відмінити");
        cmdClose.setFont(font);
        bupdate = new JButton("Редагувати");
        bupdate.setFont(font);
        getRootPane().setDefaultButton(cmdClose);
        setSize(970, 380);
        setResizable(true);
        JToolBar tools = new JToolBar();
        tools.setFloatable(false);
        getContentPane().add(mainPanel, BorderLayout.CENTER);
        getContentPane().add(tools, BorderLayout.SOUTH);

        tools.add(bupdate);
        bupdate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                updateMark();
            }
        });
        mTable.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                if (me.isPopupTrigger())
                    popupMenu.show(me.getComponent(), me.getX(), me.getY());
            }

            public void mouseReleased(MouseEvent me) {
                if (me.isPopupTrigger())
                    popupMenu.show(me.getComponent(), me.getX(), me.getY());
            }

        });
        bremove = new JButton("Видалити");
        bremove.setFont(font);
        tools.add(bremove);
        bremove.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeMark();
            }
        });
        bprint = new JButton("Надрукувати");
        bprint.setFont(font);
        tools.add(bprint);
        bprint.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printMark();
            }
        });
        mTable.getTableHeader().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int col = mTable.columnAtPoint(e.getPoint());
                orderSMarks(col);

            }
        });
    }

    private void createMenu() {
        Font fontMenu = new Font(Font.MONOSPACED, Font.PLAIN, 14);

        JMenuBar menuBar = new JMenuBar();

        JMenu mFile = new JMenu("Файл");
        JMenu mRead = new JMenu("Правка");

        updateMark = new JMenuItem("Внести зміни");
        updateMark.setToolTipText("Внести зміни у вже створений запис");
        updateMark.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U,
                ActionEvent.CTRL_MASK));
        updateMark.addActionListener(this);
        updateMark.setFont(fontMenu);
        mRead.add(updateMark);

        removeMark = new JMenuItem("Видалити оцінку");
        removeMark.setToolTipText("Видалити запис у базі");
        removeMark.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D,
                ActionEvent.SHIFT_MASK));
        removeMark.setFont(fontMenu);
        removeMark.addActionListener(this);
        mRead.add(removeMark);


        printMark = new JMenuItem("Вивести на друк");
        printMark.setToolTipText("Роздрукувати інформацію з таблиці");
        printMark.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P,
                ActionEvent.CTRL_MASK));
        printMark.addActionListener(this);
        mFile.add(printMark);
        printMark.setFont(fontMenu);
        mFile.addSeparator();


        onClose = new JMenuItem("Закрити");
        onClose.setToolTipText("Закрити таблицю");
        onClose.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4,
                ActionEvent.ALT_MASK));
        onClose.setFont(fontMenu);
        onClose.addActionListener(this);
        mFile.add(onClose);



        menuBar.add(mFile);
        menuBar.add(mRead);

        setJMenuBar(menuBar);
    }

    private void orderSMarks(int col) {
        switch (col) {
            case 0: {
                i1++;
                if (i1 % 2 == 0 && i1 != 0) {
                    marksTableModel = getTableModel("mark", "asc");
                    mTable.setModel(marksTableModel);
                    System.out.println("mark" + "asc");
                    System.out.println("kilkist " + marksTableModel.getRowCount());
                } else if (i1 != 0 && i1 % 2 != 0) {
                    marksTableModel = getTableModel("mark", "desc");
                    System.out.println("kilkist "+marksTableModel.getRowCount());
                    System.out.println("mark" + "desc");
                    mTable.setModel(marksTableModel);
                }

            }break;

            case 1: {
                i2++;
                if (i2 % 2 == 0 && i2 != 0){
                    marksTableModel = getTableModel("date", "asc");
                    System.out.println("kilkist "+marksTableModel.getRowCount());
                    mTable.setModel(marksTableModel);
                    System.out.println("date" + "asc");

                } else if (i2 != 0 && (i2 % 2 != 0)) {

                    marksTableModel = getTableModel("date", "desc");
                    mTable.setModel(marksTableModel);
                    System.out.println("kilkist "+marksTableModel.getRowCount());
                    System.out.println("date" + "desc");

                }

            }
            break;
            case 2: {
                i3++;
                if (i3 % 2 == 0 && i3 != 0) {

                    marksTableModel = getTableModel("classes_type", "asc");
                    System.out.println("kilkist "+marksTableModel.getRowCount());
                    mTable.setModel(marksTableModel);
                    System.out.println("classes_type" + "asc");
                } else if (i3 != 0 && (i3 % 2 != 0)) {
                    marksTableModel = getTableModel("classes_type", "desc");
                    System.out.println("kilkist "+marksTableModel.getRowCount());
                    mTable.setModel(marksTableModel);
                    System.out.println("classes_type" + "desc");
                }

            }
            break;
            case 3: {
                i4++;
                if (i4 % 2 == 0 && i4 != 0) {
                    marksTableModel = getTableModel("classes_type", "asc");
                    System.out.println("kilkist "+marksTableModel.getRowCount());
                    mTable.setModel(marksTableModel);
                } else if (i4 != 0) {
                    marksTableModel = getTableModel("classes_type", "desc");
                    System.out.println("kilkist "+marksTableModel.getRowCount());
                    mTable.setModel(marksTableModel);

                }

            }
            break;
            case 4: {
                i5++;
                if (i5 % 2 == 0 && i5 != 0) {

                    marksTableModel = getTableModel("retake", "asc");
                    System.out.println("kilkist "+marksTableModel.getRowCount());
                    mTable.setModel(marksTableModel);
                } else if (i5 != 0) {
                    marksTableModel = getTableModel("retake", "desc");
                    System.out.println("kilkist "+marksTableModel.getRowCount());
                    mTable.setModel(marksTableModel);
                }

            }
            break;
            case 5: {
                i6++;
                if (i6 % 2 == 0 && i6 != 0) {
                    marksTableModel = getTableModel("student_id", "asc");
                    System.out.println("kilkist "+marksTableModel.getRowCount());
                    mTable.setModel(marksTableModel);
                } else if (i6 != 0) {
                    marksTableModel = getTableModel("student_id", "desc");
                    System.out.println("kilkist "+marksTableModel.getRowCount());
                    mTable.setModel(marksTableModel);
                }

            }
            break;
            case 6: {
                i7++;
                if (i7 % 2 == 0 && i7 != 0) {
                    marksTableModel = getTableModel("subject_id", "asc");
                    System.out.println("kilkist "+marksTableModel.getRowCount());
                    mTable.setModel(marksTableModel);
                } else if (i7 != 0) {
                    marksTableModel = getTableModel("subject_id", "desc");
                    System.out.println("kilkist "+marksTableModel.getRowCount());
                    mTable.setModel(marksTableModel);
                }

            }
            break;

        }
    }

    private void removeMark() {
        int index = mTable.getSelectedRow();
        if (index == -1) {
            JOptionPane.showMessageDialog(this,
                    "Виберіть оцінку");
            return;
        }
        if (JOptionPane.showConfirmDialog(StudentsMarksForm.this,
                "Ви дійсно бажаєте видалити оцінку?",
                "Message", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {


            try {
                Mark m = marksTableModel.getRowMark(index);
                if (m != null) {
                    markManager.removeMark(m.getId().intValue());
                    marksTableModel.removeRow(index);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(StudentsMarksForm.this, e.getMessage());
            }
        }


    }

    private void printMark() {
        try {
            MessageFormat headerFormat = new MessageFormat("Сторінка {0}");
            MessageFormat footerFormat = new MessageFormat("- {0} -");
            mTable.print(JTable.PrintMode.FIT_WIDTH, headerFormat,
                    footerFormat);
        } catch (PrinterException pe) {
            System.err.println("Неможливо роздрукувати: "
                    + pe.getMessage());
        }
    }

    private void updateMark() {
        int index = mTable.getSelectedRow();
        if (index == -1) {
            JOptionPane.showMessageDialog(this,
                    "Виберіть оцінку");
            return;
        }
        Mark mark = marksTableModel.getRowMark(index);
        if (mark != null) {
            Student student = new Student();
            if (mark.getStudentId() != null)
                try {
                    student = studentManager.findById(mark.getStudentId().intValue());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            newMark.setMark(mark);
            newMark.setStudent(student);
            newMark.setVisible(true);

        }
        marksTableModel.refreshUpdatedTable();
    }

    private MarksTableModel getTableModel(String columnname, String orderVal) {
        try {
            final List<Mark> marks = markManager.getSortedMarks(columnname, orderVal);

            return new MarksTableModel(marks);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Помилка при заповненні: " + e.getMessage());
        }
        return new MarksTableModel(new ArrayList<Mark>(0));
    }

    public void actionPerformed(ActionEvent e) {
         if (e.getSource() == updateMark) {
            updateMark();
        } else if (e.getSource() == removeMark) {
            removeMark();
        }

         else if (e.getSource() == printMark) {
            printMark();

        }  else if (e.getSource() == onClose) {
            if (JOptionPane.showConfirmDialog(StudentsMarksForm.this,
                    "Ви впевнені що хочете вийти?",
                    "Увага!", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
               StudentsMarksForm.this.setVisible(false);
        }
    }
}
