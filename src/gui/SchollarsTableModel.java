package gui;

import domain.Scholar;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * Created by admin on 29.04.15.
 */
public class SchollarsTableModel extends AbstractTableModel {
    List<Scholar> records;
    private String[] columns = new String[]{"Прізвище", "Ім'я",
            "Залікова кн.", "Середній бал", "Стипендія"};

    public SchollarsTableModel(List<Scholar> records) {
        this.records = records;
    }

    @Override
    public int getRowCount() {
        return records.size();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    public String getColumnName(int columnIndex) {
        return columns[columnIndex];
    }

    public void refreshUpdatedTable() {
        fireTableRowsUpdated(0, records.size());
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Scholar s = records.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return s.getLastname();
            case 1:
                return s.getFirstname();
            case 2:
                return s.getRecordBook();
            case 3:
                return s.getAvgMark().toString();
            case 4:
                return s.getScholarship().toString();
        }
        return "";
    }
}
