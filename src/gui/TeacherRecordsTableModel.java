package gui;

import domain.TeacherRecord;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * Created by admin on 14.04.15.
 */
public class TeacherRecordsTableModel extends AbstractTableModel {
    private String[] columns = new String[]{"Прізвище", "Ім'я", "По-батькові", "Оцінка", "Дата", "Перездача"};
    private List<TeacherRecord> records;

    public TeacherRecordsTableModel(List<TeacherRecord> records) {
        this.records = records;
    }

    public String getColumnName(int columnIndex) {
        return columns[columnIndex];
    }

    public TeacherRecord getRowRecord(int rowIndex) {
        return records.get(rowIndex);
    }

    public void refreshUpdatedTable() {
        fireTableRowsUpdated(0, records.size());
    }

    /**
     * Returns the number of rows in the model. A
     * <code>JTable</code> uses this method to determine how many rows it
     * should display.  This method should be quick, as it
     * is called frequently during rendering.
     *
     * @return the number of rows in the model
     * @see #getColumnCount
     */
    @Override
    public int getRowCount() {
        return records.size();
    }

    /**
     * Returns the number of columns in the model. A
     * <code>JTable</code> uses this method to determine how many columns it
     * should create and display by default.
     *
     * @return the number of columns in the model
     * @see #getRowCount
     */
    @Override
    public int getColumnCount() {
        {
            return columns.length;
        }
    }

    /**
     * Returns the value for the cell at <code>columnIndex</code> and
     * <code>rowIndex</code>.
     *
     * @param rowIndex    the row whose value is to be queried
     * @param columnIndex the column whose value is to be queried
     * @return the value Object at the specified cell
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        TeacherRecord record = records.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return record.getLastname();
            case 1:
                return record.getFirstname();
            case 2:
                return record.getMiddlename();
            case 3:
                return Integer.toString(record.getMark());
            case 4:
                return record.getDate();
            case 5:
                return record.getRetake();
        }
        return "";
    }
}
