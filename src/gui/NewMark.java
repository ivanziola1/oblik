package gui;

import domain.Mark;
import domain.Student;
import domain.Subject;
import manager.*;

import javax.swing.*;
import javax.swing.text.MaskFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NewMark extends JDialog {

    /**
     * default
     */
    String windows = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
    private static final long serialVersionUID = 1L;
    private Student student;
    private Subject subject;
    private Mark mark;
    SubjectManagerInterface subjectManager;
    MarkManagerInterface markManager;
    StudentManagerInterface studentManager;
    private static final DateFormat df = new SimpleDateFormat("yyyy-mm-dd");
    JFormattedTextField markDate;
    private JButton cmdSave, cmdCancel;
    private JTextField markText, retakeText;
    private JLabel jLabel_1, jLabel_2, jLabel_3, jLabel_4, jLabel_5, jLabel_6;
    private JComboBox classTypeList, subjectsList;
    private List<Subject> subjects;
    private List<Student> students;

    @SuppressWarnings({"unchecked", "rawtypes"})
    public NewMark() {


        try {
            UIManager.setLookAndFeel(windows);
        } catch (Exception e2) {

            e2.printStackTrace();
        }

        subjectManager = new RemoteSubjectManagerProxy();
        markManager = new RemoteMarkManagerProxy();
        studentManager = new RemoteStudentManagerProxy();
        student = new Student();
        try {
            subjects = subjectManager.getSortedSubjects("id","desc");
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(
                    this,
                    "Помилка при заповненні: "
                            + e.getMessage());
            e.printStackTrace();
        }
        setTitle("Оцінка");
        setSize(350, 260);
        setModal(true);
        setResizable(false);
        cmdSave = new JButton("Зберегти");
        cmdCancel = new JButton("Скасувати");
        markText = new JTextField(5);
        retakeText = new JTextField(5);
        markDate = new JFormattedTextField(df);
        markDate.setColumns(20);
        try {
            MaskFormatter dateMask = new MaskFormatter("####-##-##");
            dateMask.install(markDate);
        } catch (ParseException ex) {
            Logger.getLogger(MaskFormatter.class.getName()).log(Level.SEVERE,
                    null, ex);
        }
        classTypeList = new JComboBox(new Object[]{"лекція",
                "лабораторна робота", "практична робота"});
        classTypeList.setEditable(false);
        subjectsList = new JComboBox(subjects.toArray());
        subjectsList.setEditable(false);
        JPanel newMarkMainPanel = new JPanel();
        final JPanel fieldsPanel = new JPanel(new GridLayout(5, 2, 2, 10));
        final JPanel fieldsPanelBorder = new JPanel(new FlowLayout(
                FlowLayout.CENTER, 2, 2));
        fieldsPanel.setOpaque(false);
        fieldsPanelBorder.setOpaque(false);
        fieldsPanelBorder.add(fieldsPanel);
        jLabel_1 = new JLabel("Предмет");
        jLabel_2 = new JLabel("Оцінка");
        jLabel_3 = new JLabel("Дата");
        jLabel_4 = new JLabel("Тип заняття");
        jLabel_5 = new JLabel("Перездача");
        jLabel_6 = new JLabel("Студент");
        fieldsPanel.add(jLabel_1);
        fieldsPanel.add(subjectsList);
        fieldsPanel.add(jLabel_2);
        fieldsPanel.add(markText);
        fieldsPanel.add(jLabel_3);
        fieldsPanel.add(markDate);
        fieldsPanel.add(jLabel_4);
        fieldsPanel.add(classTypeList);
        fieldsPanel.add(jLabel_5);
        fieldsPanel.add(retakeText);
        final JPanel commandsPanel = new JPanel(new FlowLayout());
        final JPanel commandsPanelBorder = new JPanel(new FlowLayout(
                FlowLayout.CENTER, 0, 0));
        commandsPanel.setOpaque(false);
        commandsPanelBorder.setOpaque(false);
        commandsPanelBorder.add(commandsPanel);
        commandsPanel.add(cmdSave);
        commandsPanel.add(cmdCancel);
        newMarkMainPanel.add(fieldsPanelBorder, BorderLayout.NORTH);
        newMarkMainPanel.add(commandsPanelBorder, BorderLayout.SOUTH);
        Container c = getContentPane();
        c.add(newMarkMainPanel);
        cmdSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                saveMark();
            }
        });

        cmdCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cancelSave();
            }
        });
    }

    void setStudent(Student student) {
        this.student = student;
    }

    void setMark(Mark mark) {
        this.mark = mark;
        markDate.setText(mark.getDate());
        if (mark.getMark() != null) {
            if (mark.getMark() == mark.getRetake())
                retakeText.setText(null);
            markText.setText(mark.getMark().toString());
        }
        classTypeList.setSelectedItem(mark.getClassesType());
        if (mark.getRetake() != null)
            retakeText.setText(mark.getRetake().toString());
        if (mark.getSubjectId() != null)
            try {
                subject = subjectManager.findById(mark.getSubjectId().intValue());
            } catch (Exception e) {
                e.printStackTrace();
            }

        subjectsList.setSelectedItem(subject);
    }

    private void saveMark() {
        try {
            this.mark.setDate(markDate.getText());
            this.mark.setMark(Integer.valueOf(markText.getText()));
            this.mark.setClassesType((String) classTypeList.getSelectedItem());
            this.mark.setStudentId(student.getId());
            if (retakeText.getText() == null || retakeText.getText().equals(""))
                this.mark.setRetake(Integer.valueOf(markText.getText()));
            else
                this.mark.setRetake(Integer.valueOf(retakeText.getText()));
            Subject s = (Subject) subjectsList.getSelectedItem();
            this.mark.setSubjectId(s.getId());
            if (mark.getId() == null) {
                int newId = markManager.createMark(mark);
                mark.setId(newId);

            } else {
                markManager.updateMark(mark);

            }

            this.setVisible(false);

        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Помилка при збереженні: " + e.getMessage());
        } finally {
            this.mark = null;
        }

    }

    private void cancelSave() {
        this.setVisible(false);
    }
}
