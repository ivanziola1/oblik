package gui;

import domain.Mark;
import domain.Student;
import domain.Subject;
import manager.RemoteStudentManagerProxy;
import manager.RemoteSubjectManagerProxy;
import manager.StudentManagerInterface;
import manager.SubjectManagerInterface;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class MarksTableModel extends AbstractTableModel {

    private String[] columns = new String[]{"Оцінка","Дата","Тип занять", "перездача", "Студент","Предмет"};
    List<Mark> marks;
    Student student;
   Subject subject;
    StudentManagerInterface studentManager;
    SubjectManagerInterface subjectManager;

    public MarksTableModel(List<Mark> marks) {
        studentManager = new RemoteStudentManagerProxy();
        subjectManager = new RemoteSubjectManagerProxy();
        this.marks = marks;
    }

    public void addMark(Mark mark) {
        marks.add(mark);
        fireTableRowsInserted(0, marks.size());
    }

    public Mark getRowMark(int rowIndex) {
        return marks.get(rowIndex);
    }

    public String getColumnName(int columnIndex) {
        return columns[columnIndex];
    }

    public void removeRow(int rowIndex) {
        marks.remove(rowIndex);
        fireTableRowsDeleted(rowIndex, rowIndex);
    }

    public void refreshUpdatedTable() {
        fireTableRowsUpdated(0, marks.size());
    }

    @Override
    public int getColumnCount() {

        return columns.length;
    }

    @Override
    public int getRowCount() {
        return marks.size();
    }
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Mark mark = marks.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return Integer.toString(mark.getMark());
            case 1:
                return mark.getDate();
            case 2:
                return mark.getClassesType();
            case 3:
                return mark.getRetake();
            case 4: {
                return mark.getStudentId();
            }
            case 5:

                return mark.getSubjectId();
        }
        return "";
    }

}
