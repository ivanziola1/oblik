package gui;

import domain.Mark;
import domain.Student;
import manager.RemoteStudentManagerProxy;
import manager.StudentManagerInterface;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.print.PrinterException;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class MainForm extends JFrame implements ActionListener {

    private static final long serialVersionUID = -1286577455252661221L;
    String windows = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
    private JButton cmdClose;
    private JTable studTable;
    private TeacherRecordsForm teacherRecordsForm;
    JPopupMenu popupMenu = new JPopupMenu();
    private JPanel imagePanel;
    private ImageIcon img;
    public JLabel limg = new JLabel();
    JButton bnew, bupdate, bremove, baddMark, bMarks, bTeacherRecords, bprint, bSubjects, bSchollars;
    int i1 = 0;
    int i2 = 0;
    int i3 = 0;
    int i4 = 0;
    int i5 = 0;
    int i6 = 0;
    int i7 = 0;
    int i8 = 0;
    int i9 = 0;
    int i10 = 0;
    int i11 = 0;
    int i12 = 0;
    private StudentsTableModel studentsTableModel;
    private StudentManagerInterface studentManager = new RemoteStudentManagerProxy();
    private NewStudent newStudent;
    private NewMark newMark;

    JTextField groupText = new JTextField();
    private StudentsAVGMarksForm studentsAVGMarksForm;
    private SchollarsForm schollarsForm;
    private StudentsMarksForm studentsMarksForm;
    JMenuItem addStudent, updateStudent, removeStudent, printStudent, newMarkt, onClose, marks, bStudentsAVGMarks, subjects, schollars, teacherRecords;

    SubjectsForm subjectsForm;


    public MainForm() {
        super();
        try {
            UIManager.setLookAndFeel(windows);
        } catch (Exception e2) {
            //
            e2.printStackTrace();
        }
        createMenu();
        newStudent = new NewStudent();
        UIManager.put("OptionPane.yesButtonText", "Так");
        UIManager.put("OptionPane.noButtonText", "Ні");
        setTitle("Облік");

        JMenuItem cmdKmNew = new JMenuItem("Новий запис");
        cmdKmNew.addActionListener(this);
        popupMenu.add(cmdKmNew);

        cmdKmNew.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                addStudent();
            }
        });


        JMenuItem cmdKmUpdate = new JMenuItem("Редагувати запис");
        cmdKmUpdate.addActionListener(this);
        popupMenu.add(cmdKmUpdate);

        cmdKmUpdate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                updateStudent();
            }
        });

        JMenuItem cmdKmRemove = new JMenuItem("Видалити запис");
        cmdKmRemove.addActionListener(this);
        popupMenu.add(cmdKmRemove);

        cmdKmRemove.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                removeStudent();
            }
        });

        popupMenu.addSeparator();
        JMenuItem cmdKmOpenOrders = new JMenuItem("Додати оцінку студенту");
        cmdKmOpenOrders.addActionListener(this);
        popupMenu.add(cmdKmOpenOrders);

        cmdKmOpenOrders.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    addMarkToStudent();
                } catch (Exception e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
        cmdClose = new JButton("Закрити");

        studentsTableModel = getTableModel("");

        studTable = new JTable(studentsTableModel);

        studTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        studTable.setPreferredScrollableViewportSize(new Dimension(850, 550));
        studTable.setRowHeight(20);
        studTable.setRowSelectionInterval(0, 0);
        studTable.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    updateStudent();
                }
            }
        });
        JScrollPane scrollPane = new JScrollPane(studTable);
        scrollPane.setOpaque(false);
        scrollPane.getViewport().setOpaque(false);

        final JPanel mainPanel = new JPanel();
        mainPanel.add(scrollPane);
        getRootPane().setDefaultButton(cmdClose);
        setResizable(false);
        JToolBar tools = new JToolBar();

        JPanel searchPanel = new JPanel(new GridLayout(3, 1, 3, 3));
        JButton cancelFindByGroup = new JButton("Скасувати пошук");
        cancelFindByGroup.setFont(new Font("Times New Roman", Font.BOLD, 17));
        JButton findByGroup = new JButton("Пошук");
        findByGroup.setFont(new Font("Times New Roman", Font.BOLD, 17));
        JLabel lab= new JLabel("Введіть групу:");
        lab.setFont(new Font("Times New Roman", Font.BOLD, 17));
        searchPanel.add(lab);
        searchPanel.add(groupText);
        searchPanel.add(findByGroup);
        searchPanel.add(cancelFindByGroup);
        searchPanel.add(Box.createVerticalGlue());
        searchPanel.add(Box.createVerticalGlue());
        cancelFindByGroup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                studentsTableModel = getTableModel("");
                studTable.setModel(studentsTableModel);
                groupText.setText("");
            }
        });
        searchPanel.setPreferredSize(new Dimension(450, 250));
        getContentPane().add(mainPanel, BorderLayout.WEST);
        getContentPane().add(tools, BorderLayout.SOUTH);
        tools.setFloatable(false);
        tools.setFont(new Font("Times New Roman", Font.BOLD, 17));
        imagePanel = new JPanel(new FlowLayout());
        limg.setPreferredSize(new Dimension(480, 360));
        limg.setFont(new Font("Times New Roman", Font.BOLD, 17));
        Box image_search = Box.createVerticalBox();
        image_search.add(imagePanel, BorderLayout.WEST);
        image_search.add(searchPanel, BorderLayout.SOUTH);
        setSize(1366, 730);
        setExtendedState(MAXIMIZED_BOTH);
        imagePanel.setOpaque(false);
        findByGroup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                findByGroup();
            }
        });
        getContentPane().add(image_search, BorderLayout.EAST);
        studTable.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                if (me.isPopupTrigger())
                    popupMenu.show(me.getComponent(), me.getX(), me.getY());
            }

            public void mouseReleased(MouseEvent me) {
                if (me.isPopupTrigger())
                    popupMenu.show(me.getComponent(), me.getX(), me.getY());
            }

            public void mouseClicked(MouseEvent me) {
                int index = studTable.getSelectedRow();
                if (index == -1)
                    return;

                try {
                    Student g = studentsTableModel.getRowStudent(index);
                    if (g != null) {
                        studentManager.getImage(g.getId());
                        img = new ImageIcon("buf.jpg");
                        img.getImage().flush();
                        limg.setIcon(img);
                        limg.setVisible(false);
                        imagePanel.add(limg);
                        limg.setOpaque(false);
                        limg.setLocation(5, 200);
                        limg.setVisible(true);
                        //
                    }
                } catch (Exception e) {
                }
            }
        });

        bnew = new JButton("Новий запис");
        bnew.setFont(new Font("Times New Roman", Font.BOLD, 17));
        bnew.setToolTipText("Додати снового студента");
        tools.add(bnew);

        bnew.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                addStudent();
            }
        });
        baddMark = new JButton("Додати оцінку");
        baddMark.setFont(new Font("Times New Roman", Font.BOLD, 17));
        baddMark.setToolTipText("Додати оцінку до обраного студента");
        tools.add(baddMark);
        baddMark.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                addMarkToStudent();
            }
        });
        bupdate = new JButton("Оновити");
        bupdate.setFont(new Font("Times New Roman", Font.BOLD, 17));
        bupdate.setToolTipText("Оновити інформацію про студента");
        tools.add(bupdate);

        bupdate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                updateStudent();
            }
        });
        bremove = new JButton("Видалити");
        bremove.setFont(new Font("Times New Roman", Font.BOLD, 17));
        bremove.setToolTipText("Видалити запис про студента з БД");
        tools.add(bremove);

        bremove.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                removeStudent();
            }
        });
        bprint = new JButton("Надрукувати");
        bprint.setFont(new Font("Times New Roman", Font.BOLD, 17));
        bprint.setToolTipText("Вивести на друк інформацію про студентів");
        tools.add(bprint);
        bprint.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printStudent();
            }
        });
        tools.setFloatable(false);
        bMarks = new JButton("Оцінки");
        bMarks.setFont(new Font("Times New Roman", Font.BOLD, 17));
        bMarks.setToolTipText("Оцінки");
        tools.add(bMarks);
        bMarks.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                openMarks();
            }


        });
        bSubjects = new JButton("Предмети");
        bSubjects.setFont(new Font("Times New Roman", Font.BOLD, 17));
        bSubjects.setToolTipText("Предмети");
        tools.add(bSubjects);
        bSubjects.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                subjectsForm = new SubjectsForm();
                subjectsForm.setVisible(true);
            }
        });

        bTeacherRecords = new JButton("Журнал оцінювання");
        bTeacherRecords.setFont(new Font("Times New Roman", Font.BOLD, 17));
        bTeacherRecords.setToolTipText("Відкриє список оцінок по предметах");
        tools.add(bTeacherRecords);
        tools.setFloatable(false);
        bTeacherRecords.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                openTeacherRecords();
            }
        });


        bTeacherRecords = new JButton("ССО");
        bTeacherRecords.setFont(new Font("Times New Roman", Font.BOLD, 17));
        bTeacherRecords.setToolTipText("Вивести список середніх оцінок студентів по предметах");
        tools.add(bTeacherRecords);
        bTeacherRecords.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                studentsAVGMarksForm = new StudentsAVGMarksForm();
                studentsAVGMarksForm.setVisible(true);
            }
        });


        bSchollars = new JButton("Стипендіати");
        bSchollars.setFont(new Font("Times New Roman", Font.BOLD, 17));
        bSchollars.setToolTipText("Відрити список стипендіатів");
        tools.add(bSchollars);
        bSchollars.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                schollarsForm = new SchollarsForm();
                schollarsForm.setVisible(true);
            }
        });
        studTable.getTableHeader().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int col = studTable.columnAtPoint(e.getPoint());
                orderStudents(col);
                groupText.setText("");

            }
        });
    }

    void addMarkToStudent() {
        int index = studTable.getSelectedRow();
        if (index == -1) {
            JOptionPane.showMessageDialog(MainForm.this, "Виберіть студента");
            return;
        }

        Student student = studentsTableModel.getRowStudent(index);
        if (student != null) {
            newMark = new NewMark();
            newMark.setMark(new Mark());
            newMark.setStudent(student);
            newMark.setVisible(true);

        }
        studentsTableModel.refreshUpdatedTable();
    }

    private void orderStudents(int col) {
        switch (col) {
            case 0: {
                i1++;
                if (i1 % 2 == 0 && i1 != 0) {
                    studentsTableModel = getortedTableModel("lastname", "asc");
                    System.out.println("subject_name" + "asc");
                    studTable.setModel(studentsTableModel);
                } else if (i1 != 0 && i1 % 2 != 0) {
                    studentsTableModel = getortedTableModel("lastname", "desc");
                    studTable.setModel(studentsTableModel);
                    System.out.println("subject_name" + "desc");
                }

            }
            break;
            case 1: {
                i2++;
                if (i2 % 2 == 0 && i2 != 0) {
                    studentsTableModel = getortedTableModel("firstname", "asc");
                    studTable.setModel(studentsTableModel);
                } else if (i2 != 0 && (i2 % 2 != 0)) {
                    studentsTableModel = getortedTableModel("firstname", "desc");
                    studTable.setModel(studentsTableModel);
                }

            }
            break;
            case 2: {
                i3++;
                if (i3 % 2 == 0 && i3 != 0) {
                    studentsTableModel = getortedTableModel("middlename", "asc");
                    studTable.setModel(studentsTableModel);
                } else if (i3 != 0 && (i3 % 2 != 0)) {
                    studentsTableModel = getortedTableModel("middlename", "desc");
                    studTable.setModel(studentsTableModel);
                }

            }
            break;
            case 3: {
                i4++;
                if (i4 % 2 == 0 && i4 != 0) {
                    studentsTableModel = getortedTableModel("birthday_date", "asc");
                    studTable.setModel(studentsTableModel);
                } else if (i4 != 0) {
                    studentsTableModel = getortedTableModel("birthday_date", "desc");
                    studTable.setModel(studentsTableModel);

                }

            }
            break;
            case 4: {
                i5++;
                if (i5 % 2 == 0 && i5 != 0) {

                    studentsTableModel = getortedTableModel("course", "asc");
                    studTable.setModel(studentsTableModel);
                } else if (i5 != 0) {
                    studentsTableModel = getortedTableModel("course", "desc");
                    studTable.setModel(studentsTableModel);
                }

            }
            break;
            case 5: {
                i6++;
                if (i6 % 2 == 0 && i6 != 0) {
                    studentsTableModel = getortedTableModel("`group`", "asc");
                    studTable.setModel(studentsTableModel);
                } else if (i6 != 0) {
                    studentsTableModel = getortedTableModel("`group`", "desc");
                    studTable.setModel(studentsTableModel);
                }

            }
            break;
            case 6: {
                i7++;
                if (i7 % 2 == 0 && i7 != 0) {
                    studentsTableModel = getortedTableModel("curator", "asc");
                    studTable.setModel(studentsTableModel);
                } else if (i7 != 0) {
                    studentsTableModel = getortedTableModel("curator", "desc");
                    studTable.setModel(studentsTableModel);
                }

            }
            break;
            case 7: {
                i8++;
                if (i8 % 2 == 0 && i8 != 0) {
                    studentsTableModel = getortedTableModel("record_book", "asc");
                    studTable.setModel(studentsTableModel);
                } else if (i8 != 0) {
                    studentsTableModel = getortedTableModel("record_book", "desc");
                    studTable.setModel(studentsTableModel);
                }

            }
            break;
            case 8: {
                i9++;
                if (i9 % 2 == 0 && i9 != 0) {
                    studentsTableModel = getortedTableModel("sex", "asc");
                    studTable.setModel(studentsTableModel);
                } else if (i9 != 0) {
                    studentsTableModel = getortedTableModel("sex", "desc");
                    studTable.setModel(studentsTableModel);
                }

            }
            break;
            case 9: {
                i10++;
                if (i10 % 2 == 0 && i10 != 0) {
                    studentsTableModel = getortedTableModel("studying_type", "asc");
                    studTable.setModel(studentsTableModel);
                } else if (i10 != 0) {
                    studentsTableModel = getortedTableModel("studying_type", "desc");
                    studTable.setModel(studentsTableModel);
                }

            }
            break;
            case 10: {
                i11++;
                if (i11 % 2 == 0 && i11 != 0) {
                    studentsTableModel = getortedTableModel("tuition", "asc");
                    studTable.setModel(studentsTableModel);
                } else if (i11 != 0) {
                    studentsTableModel = getortedTableModel("tuition", "desc");
                    studTable.setModel(studentsTableModel);
                }

            }
            break;
        }
    }

    private void findByGroup() {
        if (groupText.getText().equals(""))
            JOptionPane.showMessageDialog(MainForm.this, "Введіть групу");
        else {
            studentsTableModel = getTableModel(groupText.getText());
            studTable.setModel(studentsTableModel);


        }
    }

    private void addStudent() {
        newMark = new NewMark();
        newStudent.setStudent(new Student());
        newStudent.setVisible(true);
        if (newStudent.getStudent().getId() != null) {
            studentsTableModel.addStudent(newStudent.getStudent());
        }

    }

    private void openMarks() {
        studentsMarksForm = new StudentsMarksForm();
        studentsMarksForm.setVisible(true);
    }

    private void printStudent() {
        try {
            MessageFormat headerFormat = new MessageFormat("Сторінка {0}");
            MessageFormat footerFormat = new MessageFormat("- {0} -");
            studTable.print(JTable.PrintMode.FIT_WIDTH, headerFormat,
                    footerFormat);
        } catch (PrinterException pe) {
            System.err.println("Неможливо роздрукувати документ по причині: "
                    + pe.getMessage());
        }
    }

    private StudentsTableModel getTableModel(String group) {
        try {
            final List<Student> groups = studentManager.findByGroup(group);

            return new StudentsTableModel(groups);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Помилка при заповненні таблиці: " + e.getMessage());
        }
        return new StudentsTableModel(new ArrayList<Student>(0));
    }

    private StudentsTableModel getortedTableModel(String columnname, String orderVal) {
        try {
            final List<Student> groups = studentManager.getSortedSubjects(columnname, orderVal);

            return new StudentsTableModel(groups);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Помилка при заповненні таблиці: " + e.getMessage());
        }
        return new StudentsTableModel(new ArrayList<Student>(0));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == addStudent) {
            addStudent();
        } else if (e.getSource() == updateStudent) {
            updateStudent();
        } else if (e.getSource() == removeStudent) {
            removeStudent();
        } else if (e.getSource() == printStudent) {
            printStudent();

        } else if (e.getSource() == subjects) {
            subjectsForm = new SubjectsForm();
            subjectsForm.setVisible(true);
        } else if (e.getSource() == schollars) {
            schollarsForm = new SchollarsForm();
            schollarsForm.setVisible(true);
        } else if (e.getSource() == bStudentsAVGMarks) {
            studentsAVGMarksForm = new StudentsAVGMarksForm();
            studentsAVGMarksForm.setVisible(true);
        } else if (e.getSource() == marks) {
            studentsMarksForm = new StudentsMarksForm();
            studentsMarksForm.setVisible(true);
        } else if (e.getSource() == teacherRecords) {
            teacherRecordsForm = new TeacherRecordsForm();
            teacherRecordsForm.setVisible(true);
        } else if (e.getSource() == newMarkt) {
            addMarkToStudent();
        } else if (e.getSource() == marks) {
            openMarks();
        } else if (e.getSource() == onClose) {
            if (JOptionPane.showConfirmDialog(MainForm.this,
                    "Ви впевнені що хочете вийти?",
                    "Увага!", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
                System.exit(0);
        }
    }

    void createMenu() {
        Color colorMenu = (Color.GRAY);
        Font fontMenu = new Font(Font.MONOSPACED, Font.PLAIN, 14);

        JMenuBar menuBar = new JMenuBar();

        JMenu mFile = new JMenu("Файл");
        JMenu mRead = new JMenu("Правка");
        JMenu mTabl = new JMenu("Таблиці і звіти");


        addStudent = new JMenuItem("Додати студента");
        addStudent.setToolTipText("Додати новий запис до бази");
        addStudent.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,
                ActionEvent.CTRL_MASK));
        addStudent.setFont(fontMenu);
        addStudent.addActionListener(this);
        mRead.add(addStudent);

        newMarkt = new JMenuItem("Додати оцінку");
        newMarkt.setToolTipText("Додати оцінку обраному студенту");
        newMarkt.setFont(fontMenu);
        newMarkt.addActionListener(this);
        mRead.add(newMarkt);

        updateStudent = new JMenuItem("Внести зміни");
        updateStudent.setToolTipText("Внести зміни у вже створений запис");
        updateStudent.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U,
                ActionEvent.CTRL_MASK));
        updateStudent.addActionListener(this);
        updateStudent.setFont(fontMenu);
        mRead.add(updateStudent);
        removeStudent = new JMenuItem("Видалити студента");
        removeStudent.setToolTipText("Видалити запис у базі");
        removeStudent.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D,
                ActionEvent.SHIFT_MASK));
        removeStudent.setFont(fontMenu);
        removeStudent.addActionListener(this);
        mRead.add(removeStudent);


        printStudent = new JMenuItem("Вивести на друк");
        printStudent.setToolTipText("Роздрукувати інформацію з таблиці");
        printStudent.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P,
                ActionEvent.CTRL_MASK));
        printStudent.addActionListener(this);
        mFile.add(printStudent);
        printStudent.setFont(fontMenu);
        mFile.addSeparator();


        onClose = new JMenuItem("Вихід");
        onClose.setToolTipText("Вийти з програми");
        onClose.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4,
                ActionEvent.ALT_MASK));
        onClose.setFont(fontMenu);
        onClose.addActionListener(this);
        mFile.add(onClose);

        subjects = new JMenuItem("Предмети");
        subjects.setToolTipText("Предмети");

        subjects.setFont(fontMenu);
        subjects.addActionListener(this);
        mTabl.add(subjects);
        marks = new JMenuItem("Оцінки");
        marks.setFont(fontMenu);
        marks.addActionListener(this);
        mTabl.add(marks);


        schollars = new JMenuItem("Стипендіати");
        schollars.setFont(fontMenu);
        schollars.addActionListener(this);
        mTabl.add(schollars);


        bStudentsAVGMarks = new JMenuItem("ССО");
        bStudentsAVGMarks.setToolTipText("Вивести список середніх оцінок студентів по предметах");
        bStudentsAVGMarks.addActionListener(this);
        mTabl.add(bStudentsAVGMarks);

        teacherRecords = new JMenuItem("Журнал оцінювання");
        teacherRecords.setToolTipText("Журнал оцінювання");
        teacherRecords.addActionListener(this);
        mTabl.add(teacherRecords);

        menuBar.add(mFile);
        menuBar.add(mRead);
        menuBar.add(mTabl);
        setJMenuBar(menuBar);
    }

    private void updateStudent() {
        int index = studTable.getSelectedRow();
        if (index == -1) {
            JOptionPane.showMessageDialog(this,
                    "Виберіть студента");
            return;
        }
        Student student = studentsTableModel.getRowStudent(index);
        if (student != null) {
            newStudent.setStudent(student);
            newStudent.setVisible(true);

        }
        studentsTableModel.refreshUpdatedTable();
    }


    private void openTeacherRecords() {
        teacherRecordsForm = new TeacherRecordsForm();
        teacherRecordsForm.setVisible(true);
    }

    private void removeStudent() {
        int index = studTable.getSelectedRow();
        if (index == -1) {
            JOptionPane.showMessageDialog(this,
                    "Виберіть студента");
            return;

        }
        if (JOptionPane.showConfirmDialog(MainForm.this,
                "Ви дійсно бажаєте видалити студента?",
                "Message", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {


            try {
                Student st = studentsTableModel.getRowStudent(index);
                if (st != null) {
                    studentManager.removeStudent(st.getId());
                    studentsTableModel.removeRow(index);
                }
            } catch (SQLException e2) {
                JOptionPane.showMessageDialog(MainForm.this, "Неможливо видалити оскільки є пов'язані записи");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(MainForm.this, e.getMessage());
            }
        }

    }
}
