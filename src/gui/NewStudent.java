package gui;

import domain.Student;
import filter.ExtFileFilter;
import manager.RemoteStudentManagerProxy;
import manager.StudentManagerInterface;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class NewStudent extends JDialog {

    private static final long serialVersionUID = -7265530307974489903L;

    private Student student;
    private String filePath = "";
    private File file;
    private JTextField firstnameText, lastnameText, middlenameText, dateText,
            groupText, curatorText, recordBookText;

    private JComboBox learningformList, courseList, sexList, tuitionList;
    private JLabel jLabel_1, jLabel_2, jLabel_3, jLabel_4, jLabel_5, jLabel_6,
            jLabel_7, jLabel_8, jLabel_9, jLabel_10, jLabel_11, jLabel_12;
    private JButton openChoser;
    private StudentManagerInterface studentManager;

    @SuppressWarnings({"unchecked", "rawtypes"})
    public NewStudent() {
        JFrame.setDefaultLookAndFeelDecorated(true);
        JDialog.setDefaultLookAndFeelDecorated(true);
        setTitle("Студент");
        setSize(500, 500);
        setModal(true);
        setResizable(false);
        studentManager = new RemoteStudentManagerProxy();

        final JButton cmdSave = new JButton("Зберегти");
        final JButton cmdCancel = new JButton("Відмінити");
        openChoser = new JButton("Вибрати зображення");
        firstnameText = new JTextField(20);
        lastnameText = new JTextField(20);
        middlenameText = new JTextField(20);
        dateText = new JTextField(20);
        curatorText = new JTextField(20);
        groupText = new JTextField(20);
        learningformList = new JComboBox(new Object[]{"Державна", "Платна"});
        learningformList.setEditable(false);
        courseList = new JComboBox(new Object[]{1, 2, 3, 4});
        courseList.setEditable(false);
        recordBookText = new JTextField(10);
        sexList = new JComboBox(new Object[]{"ч", "ж"});
        sexList.setEditable(false);
        tuitionList = new JComboBox(new Object[]{"Денна", "Заочна"});
        tuitionList.setEditable(false);

        JPanel NewStudentMainPanel = new JPanel();
        final JPanel fieldsPanel = new JPanel(new GridLayout(12, 2, 2, 10));
        final JPanel fieldsPanelBorder = new JPanel(new FlowLayout(
                FlowLayout.CENTER, 2, 2));
        fieldsPanel.setOpaque(false);
        fieldsPanelBorder.setOpaque(false);
        fieldsPanelBorder.add(fieldsPanel);
        jLabel_1 = new JLabel("Ім’я");
        jLabel_2 = new JLabel("Прізвище");
        jLabel_3 = new JLabel("По-батькові");
        jLabel_4 = new JLabel("Група");
        jLabel_5 = new JLabel("Курс");
        jLabel_6 = new JLabel("Номер залікової книжки");
        jLabel_7 = new JLabel("Форма навчання");
        jLabel_8 = new JLabel("Вид навчання");
        jLabel_9 = new JLabel("Дата народження");
        jLabel_10 = new JLabel("Стать");
        jLabel_11 = new JLabel("Куратор");
        jLabel_12 = new JLabel("Фото");

        fieldsPanel.add(jLabel_1);
        fieldsPanel.add(firstnameText);
        fieldsPanel.add(jLabel_2);
        fieldsPanel.add(lastnameText);
        fieldsPanel.add(jLabel_3);
        fieldsPanel.add(middlenameText);
        fieldsPanel.add(jLabel_4);
        fieldsPanel.add(groupText);
        fieldsPanel.add(jLabel_5);
        fieldsPanel.add(courseList);
        fieldsPanel.add(jLabel_6);
        fieldsPanel.add(recordBookText);
        fieldsPanel.add(jLabel_7);
        fieldsPanel.add(learningformList);
        fieldsPanel.add(jLabel_8);
        fieldsPanel.add(tuitionList);
        fieldsPanel.add(jLabel_9);
        fieldsPanel.add(dateText);
        fieldsPanel.add(jLabel_10);
        fieldsPanel.add(sexList);
        fieldsPanel.add(jLabel_11);
        fieldsPanel.add(curatorText);
        fieldsPanel.add(jLabel_12);
        fieldsPanel.add(openChoser);

        final JPanel commandsPanel = new JPanel(new FlowLayout());
        final JPanel commandsPanelBorder = new JPanel(new FlowLayout(
                FlowLayout.CENTER, 0, 0));
        commandsPanel.setOpaque(false);
        commandsPanelBorder.setOpaque(false);
        commandsPanelBorder.add(commandsPanel);
        commandsPanel.add(cmdSave);
        commandsPanel.add(cmdCancel);

        NewStudentMainPanel.add(fieldsPanelBorder, BorderLayout.NORTH);
        NewStudentMainPanel.add(commandsPanelBorder, BorderLayout.SOUTH);
        Container c = getContentPane();
        c.add(NewStudentMainPanel);
        cmdSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                saveStudent();
            }
        });

        cmdCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cancelSave();
            }
        });
        openChoser.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileopen = new JFileChooser();
                ExtFileFilter ff1 = new ExtFileFilter("jpg", "JPG Зображення");
                fileopen.addChoosableFileFilter(ff1);
                ExtFileFilter ff2 = new ExtFileFilter("bmp", "BMP Зображення");
                fileopen.addChoosableFileFilter(ff2);
                ExtFileFilter ff3 = new ExtFileFilter("jpeg", "JPEG Зображення");
                fileopen.addChoosableFileFilter(ff3);
                ExtFileFilter ff4 = new ExtFileFilter("png", "PNG Зображення");
                fileopen.addChoosableFileFilter(ff4);
                fileopen.setFileFilter(ff1);
                int ret = fileopen.showDialog(null, "Вибрати файл");
                if (ret == JFileChooser.APPROVE_OPTION) {
                    file = fileopen.getSelectedFile();
                    filePath = file.getAbsolutePath();

                }
            }
        });
    }

    public Student getStudent() {
        return student;

    }

    public void setStudent(Student student) {
        this.student = student;
        firstnameText.setText(student.getFirstname());
        lastnameText.setText(student.getLastname());
        middlenameText.setText(student.getMiddlename());
        courseList.setSelectedItem(student.getCourse());
        learningformList.setSelectedItem(student.getStudyingType());
        tuitionList.setSelectedItem(student.getTuition());
        dateText.setText(student.getBirthdayDate());
        curatorText.setText(student.getCurator());
        groupText.setText(student.getGroup());
        recordBookText.setText(student.getRecordBook());
        sexList.setSelectedItem(student.getSex());

    }

    private void saveStudent() {

        try {
            student.setFirstname(firstnameText.getText());
            student.setLastname(lastnameText.getText());
            student.setMiddlename(middlenameText.getText());
            student.setCurator(curatorText.getText());
            student.setCourse((Integer) courseList.getSelectedItem());
            student.setBirthdayDate(dateText.getText());
            student.setGroup(groupText.getText());
            student.setRecordBook(recordBookText.getText());
            student.setSex((String) sexList.getSelectedItem());
            student.setStudyingType((String) learningformList
                    .getSelectedItem());
            student.setTuition((String) tuitionList.getSelectedItem());
            if (file != null)
                student.setFileName(filePath);


            if (student.getId() == null) {
                int newId = studentManager.createStudent(student);
                student.setId(newId);
            } else {
                studentManager.updateStudent(student);
            }
            file = null;
            NewStudent.this.setVisible(false);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(NewStudent.this,
                    "Помилка при збереженні студента: " + e.getMessage());
        }
    }


    private void cancelSave() {
        this.setVisible(false);
    }

}
